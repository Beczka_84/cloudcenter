﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IPartnerService
    {
        IList<Partner> GetPartners(string UserId = null, string role = null);
        Partner GetPartner(int Id);
        ServiceResult AddPartner(Partner partner);
        ServiceResult UpdatePartner(Partner partnerToUpdate);
    }
}

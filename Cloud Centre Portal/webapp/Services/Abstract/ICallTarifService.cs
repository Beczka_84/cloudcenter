﻿using CloudCentrePortal.DAL.Model;
using System.Collections.Generic;

namespace CloudCentre.Services.Abstract
{
    public interface ICallTarifService
    {
        IList<CallTariff> GetCallTariffs();
        CallTariff GetCallTariff(int Id);
        ServiceResult AddCallTariff(CallTariff CallTariff);
        ServiceResult UpdateCallTariff(CallTariff callTariffToUpdate);
        ServiceResult DeleteCallTariff(CallTariff CallTariff);

        void RemoveCallTariff(CallTariffItem CallTariff);


    }
}

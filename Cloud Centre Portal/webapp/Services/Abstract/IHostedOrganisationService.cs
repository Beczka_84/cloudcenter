﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IHostedOrganisationService
    {
        IList<HostedOrganisation> GetHostedOrganisations(string UserId = null, string role = null);
        HostedOrganisation GetHostedOrganisation(int Id);
        ServiceResult AddHostedOrganisation(HostedOrganisation hostedOrganisation);
        ServiceResult UpdateHostedOrganisation(HostedOrganisation hostedOrganisation);

        void RemoveDomainNames(DomainNames domainNames);
        void RemoveTelephone(PhoneNumbers phoneNumbers);
    }
}

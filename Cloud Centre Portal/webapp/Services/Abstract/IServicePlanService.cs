﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
   public interface IServicePlanService
    {
        IList<ServicePlan> GetServicePlans();
        ServicePlan GetServicePlan(int Id);
        ServiceResult AddServicePlan(ServicePlan servicePlan);
        ServiceResult UpdateServicePlan(ServicePlan servicePlanToUpdate);
        ServiceResult DeleteServicePlan(ServicePlan servicePlan);
        bool CheckIfServicePlanExist(string servicePlanName, int? Id);
    }
}

﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IRoleService
    {
        IList<IdentityRole> GetAppRoles();
    }
}

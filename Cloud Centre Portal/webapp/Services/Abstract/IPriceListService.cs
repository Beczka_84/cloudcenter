﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.Services.Abstract
{
    public interface IPriceListService
    {
        IList<PriceList> GetPriceLists();
        PriceList GetPriceList(int Id);
        ServiceResult AddPriceList(PriceList priceList);
        ServiceResult UpdatePriceList(PriceList priceListToUpdate);
        ServiceResult DeletePriceList(PriceList priceList);

        void RemovePriceListItem(PriceListItem priceListItem);
    }
}

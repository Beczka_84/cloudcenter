﻿using CloudCentrePortal.DAL.Model;
using System.Collections.Generic;


namespace CloudCentre.Services.Abstract
{
    public interface IStatusService
    {
        IList<Status> GetStatuses();
        Status GetStatus(int Id);
        ServiceResult AddStatus(Status status);
        ServiceResult UpdateStatus(Status statusToUpdate);
        ServiceResult DeleteStatus(Status model);
        bool CheckIfStatusExist(string statusName, int? Id);

    }
}

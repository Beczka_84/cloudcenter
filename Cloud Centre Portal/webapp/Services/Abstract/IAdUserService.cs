﻿using System.Collections.Generic;
using CloudCentrePortal.DAL.Model;

namespace CloudCentre.Services.Abstract
{
    public interface IAdUserService
    {
        IList<ADUser> GetADUsers();
        ADUser GetADUser(int Id);
        ServiceResult AddADUser(ADUser adUser);
        ServiceResult UpdateADUser(ADUser adUser);

        void RemoveTelephone(PhoneNumbers phoneNumbers);

    }
}

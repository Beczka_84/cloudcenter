﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CloudCentre.Services
{
    public class BaseService
    {
        public List<KeyValuePair<int, string>> FormatExceptions(DbUpdateException exception)
        {
            List<KeyValuePair<int, string>> errorList = new List<KeyValuePair<int, string>>();
            if (exception.InnerException != null && exception.InnerException.InnerException != null)
            {
                SqlException sqlException = exception.InnerException.InnerException as SqlException;
                errorList.Add(new KeyValuePair<int, string>(sqlException.Number, sqlException.Message));
            }
            return errorList;
        }
    }
}
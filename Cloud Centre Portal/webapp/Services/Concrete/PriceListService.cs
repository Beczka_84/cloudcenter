﻿using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentrePortal.DAL.Model;
using CloudCentre.DAL.UnitOfWork.Abstract;
using System.Data.Entity.Infrastructure;

namespace CloudCentre.Services.Concrete
{
    public class PriceListService : BaseService, IPriceListService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PriceListService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddPriceList(PriceList priceList)
        {
            try
            {
                _unitOfWork.PriceList.Insert(priceList);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public ServiceResult DeletePriceList(PriceList priceList)
        {
            try
            {
                _unitOfWork.PriceList.Delete(priceList);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public PriceList GetPriceList(int Id)
        {
            return _unitOfWork.PriceList.GetByID(Id);
        }

        public IList<PriceList> GetPriceLists()
        {
            return _unitOfWork.PriceList.Get().ToList();
        }

        public void RemovePriceListItem(PriceListItem priceListItem)
        {
            _unitOfWork.PriceListItem.Remove(priceListItem);
        }

        public ServiceResult UpdatePriceList(PriceList priceListToUpdate)
        {
            try
            {
                _unitOfWork.PriceList.Update(priceListToUpdate);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}
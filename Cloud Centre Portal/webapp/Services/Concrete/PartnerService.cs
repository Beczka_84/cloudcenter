﻿using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentrePortal.DAL.Model;
using CloudCentre.DAL.UnitOfWork.Abstract;
using System.Data.Entity.Infrastructure;
using CloudCentre.Models;

namespace CloudCentre.Services.Concrete
{
    public class PartnerService : BaseService, IPartnerService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PartnerService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddPartner(Partner partner)
        {
            try
            {
                _unitOfWork.Partner.Insert(partner);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
        
        public Partner GetPartner(int Id)
        {
            return _unitOfWork.Partner.GetByID(Id);
        }

        public IList<Partner> GetPartners(string UserId = null, string role = null)
        {

            if (UserId == null && role == DefaultRoles.Admin || role == DefaultRoles.ServiceDesk)  //All Organisations - Admin/ServiceDesk
            {
                return _unitOfWork.Partner.Get(x => x.StatusID == (int)StatusEnum.Enabled).ToList();
            }

            if (role == DefaultRoles.Partner)   //Only Ogranisations of Partner user  - Partner
            {
                return _unitOfWork.Partner.Get(x => x.StatusID == (int)StatusEnum.Enabled && (x.HostedOrganisations.FirstOrDefault(y => y.AppUsers.FirstOrDefault(c => c.Id == UserId) != null)) != null).ToList();
            }


            if (role == DefaultRoles.Customer)
            {
                //Only Ogranisations of current user  - customer 
                return _unitOfWork.Partner.Get(x => x.StatusID == (int)StatusEnum.Enabled && (x.HostedOrganisations.FirstOrDefault(y => y.AppUsers.FirstOrDefault(c=>c.Id == UserId) != null)) != null).ToList();
            }

            return _unitOfWork.Partner.Get(x => x.StatusID == (int)StatusEnum.Enabled).ToList();
        }

        public ServiceResult UpdatePartner(Partner partnerToUpdate)
        {
            try
            {
                _unitOfWork.Partner.Update(partnerToUpdate);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}
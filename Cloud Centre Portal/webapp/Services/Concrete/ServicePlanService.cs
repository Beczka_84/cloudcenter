﻿using CloudCentre.DAL.UnitOfWork.Abstract;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace CloudCentre.Services.Concrete
{
    public class ServicePlanService : BaseService, IServicePlanService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ServicePlanService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddServicePlan(ServicePlan servicePlan)
        {
            try
            {
                _unitOfWork.ServicePlan.Insert(servicePlan);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public bool CheckIfServicePlanExist(string servicePlaName, int? Id = null)
        {
            var servicePlan = _unitOfWork.ServicePlan.Get(x => x.ID == Id).FirstOrDefault();
            if (servicePlan != null)
            {
                if (servicePlan.Name == servicePlaName) { return false; }
            }
            return _unitOfWork.ServicePlan.Get(x => x.Name == servicePlaName).Any();
        }

        public ServiceResult DeleteServicePlan(ServicePlan servicePlan)
        {
            try
            {
                _unitOfWork.ServicePlan.Delete(servicePlan);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public ServicePlan GetServicePlan(int Id)
        {
            return _unitOfWork.ServicePlan.GetByID(Id);
        }

        public IList<ServicePlan> GetServicePlans()
        {
            return _unitOfWork.ServicePlan.Get().ToList();
        }

        public ServiceResult UpdateServicePlan(ServicePlan servicePlanToUpdate)
        {
            try
            {
                _unitOfWork.ServicePlan.Update(servicePlanToUpdate);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}
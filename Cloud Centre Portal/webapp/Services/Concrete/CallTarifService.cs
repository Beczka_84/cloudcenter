﻿using CloudCentre.DAL.UnitOfWork.Abstract;
using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentrePortal.DAL.Model;
using System.Data.Entity.Infrastructure;

namespace CloudCentre.Services.Concrete
{
    public class CallTarifService : BaseService, ICallTarifService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CallTarifService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddCallTariff(CallTariff CallTariff)
        {
            try
            {
                _unitOfWork.CallTariff.Insert(CallTariff);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public ServiceResult DeleteCallTariff(CallTariff CallTariff)
        {
            try
            {
                _unitOfWork.CallTariff.Delete(CallTariff);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public CallTariff GetCallTariff(int Id)
        {
            return _unitOfWork.CallTariff.GetByID(Id);
        }

        public IList<CallTariff> GetCallTariffs()
        {
            return _unitOfWork.CallTariff.Get().ToList();
        }

        public void RemoveCallTariff(CallTariffItem CallTariff)
        {
           _unitOfWork.CallTariffItems.Remove(CallTariff);
        }

        public ServiceResult UpdateCallTariff(CallTariff callTariffToUpdate)
        {
            try
            {
                _unitOfWork.CallTariff.Update(callTariffToUpdate);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}
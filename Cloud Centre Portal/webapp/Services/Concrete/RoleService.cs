﻿using CloudCentre.DAL.UnitOfWork.Abstract;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Services.Concrete
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        public RoleService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IList<IdentityRole> GetAppRoles()
        {
            return _unitOfWork.AppRoles.Get().ToList();
        }
    }
}
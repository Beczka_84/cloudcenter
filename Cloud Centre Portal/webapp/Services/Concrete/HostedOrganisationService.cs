﻿using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentrePortal.DAL.Model;
using CloudCentre.DAL.UnitOfWork.Abstract;
using System.Data.Entity.Infrastructure;
using CloudCentre.Models;

namespace CloudCentre.Services.Concrete
{
    public class HostedOrganisationService : BaseService, IHostedOrganisationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public HostedOrganisationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddHostedOrganisation(HostedOrganisation hostedOrganisation)
        {
            try
            {
                _unitOfWork.HostedOrganisations.Insert(hostedOrganisation);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public HostedOrganisation GetHostedOrganisation(int Id)
        {
            return _unitOfWork.HostedOrganisations.GetByID(Id);
        }

        public IList<HostedOrganisation> GetHostedOrganisations(string UserId = null, string role = null)
        {

            if (UserId == null && role == DefaultRoles.Admin || role == DefaultRoles.ServiceDesk)  //All Organisations - Admin/ServiceDesk
            { 
                return _unitOfWork.HostedOrganisations.Get(x => x.StatusID == (int)StatusEnum.Enabled).ToList();
            }

            if (role == DefaultRoles.Partner)   //Only Ogranisations of Partner user  - Partner
            {
                var hostedOrganisation = _unitOfWork.HostedOrganisations.Get(x => x.StatusID == (int)StatusEnum.Enabled && (x.AppUsers.FirstOrDefault(y => y.Id == UserId)) != null).FirstOrDefault();
                return _unitOfWork.HostedOrganisations.Get(x => x.StatusID == (int)StatusEnum.Enabled && x.PartnerID == hostedOrganisation.PartnerID).ToList();
            }

            if (role == DefaultRoles.Customer)
            {
                //Only Ogranisations of current user  - customer 
                return _unitOfWork.HostedOrganisations.Get(x => x.StatusID == (int)StatusEnum.Enabled && (x.AppUsers.FirstOrDefault(y => y.Id == UserId)) != null).ToList();
            }

            return _unitOfWork.HostedOrganisations.Get(x => x.StatusID == (int)StatusEnum.Enabled).ToList();
        }

        public void RemoveDomainNames(DomainNames domainNames)
        {
            _unitOfWork.DomainNames.Remove(domainNames);
        }

        public void RemoveTelephone(PhoneNumbers phoneNumbers)
        {
            _unitOfWork.PhoneNumbers.Remove(phoneNumbers);
        }

        public ServiceResult UpdateHostedOrganisation(HostedOrganisation hostedOrganisation)
        {
            try
            {
                _unitOfWork.HostedOrganisations.Update(hostedOrganisation);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}
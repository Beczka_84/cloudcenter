﻿using CloudCentre.DAL.UnitOfWork.Abstract;
using CloudCentre.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CloudCentrePortal.DAL.Model;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using CloudCentre.Models;

namespace CloudCentre.Services.Concrete
{
    public class StatusService : BaseService, IStatusService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StatusService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddStatus(Status status)
        {
            try
            {
                _unitOfWork.Status.Insert(status);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public bool CheckIfStatusExist(string statusName, int? Id = null)
        {
            var Status = _unitOfWork.Status.Get(x => x.Id == Id).FirstOrDefault();
            if (Status != null) {
                if (Status.Text == statusName) { return false; }
            }
            return _unitOfWork.Status.Get(x => x.Text == statusName).Any();
        }

        public ServiceResult DeleteStatus(Status model)
        {
            try
            {
                _unitOfWork.Status.Delete(model);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public Status GetStatus(int Id)
        {
           return _unitOfWork.Status.GetByID(Id);
        }

        public IList<Status> GetStatuses()
        {
            return _unitOfWork.Status.Get().ToList();
        }

        public ServiceResult UpdateStatus(Status statusToUpdate)
        {
            try
            {
                _unitOfWork.Status.Update(statusToUpdate);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

    }
}
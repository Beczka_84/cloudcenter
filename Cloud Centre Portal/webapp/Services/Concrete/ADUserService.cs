﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using CloudCentre.DAL.UnitOfWork.Abstract;
using CloudCentre.Models;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;

namespace CloudCentre.Services.Concrete
{
    public class AdUserSevice : BaseService, IAdUserService
    {
        private readonly IUnitOfWork _unitOfWork;


        public AdUserSevice(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public ServiceResult AddADUser(ADUser adUser)
        {
            try
            {
                _unitOfWork.ADUser.Insert(adUser);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }

        public ADUser GetADUser(int Id)
        {
            return _unitOfWork.ADUser.GetByID(Id);
        }

        public IList<ADUser> GetADUsers()
        {
            return _unitOfWork.ADUser.Get(x => x.StatusID == (int)StatusEnum.Enabled).ToList();
        }

        public void RemoveTelephone(PhoneNumbers phoneNumbers)
        {
            _unitOfWork.PhoneNumbers.Remove(phoneNumbers);
        }

        public ServiceResult UpdateADUser(ADUser adUser)
        {
            try
            {
                _unitOfWork.ADUser.Update(adUser);
                _unitOfWork.Save();
                return ServiceResult.Ok();
            }
            catch (DbUpdateException e)
            {
                return ServiceResult.Fail(base.FormatExceptions(e));
            }
        }
    }
}
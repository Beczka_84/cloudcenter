﻿using CloudCentre.Services.Abstract;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using CloudCentre.App_Start;
using Microsoft.Owin.Security.DataProtection;
using System.Security.Claims;
using CloudCentre.Infrastructure.IdentityEmail;
using Microsoft.AspNet.Identity.EntityFramework;
using CloudCentrePortal.DAL.Model;
using System.Collections.Generic;
using CloudCentre.DAL.UnitOfWork.Abstract;

namespace CloudCentre.Services.Concrete
{
    /// <summary>
    /// This service is for user management. It contains methods to :
    /// add/remove user to role, 
    /// create/update/remove user from db, 
    /// signin/signout user form web site, 
    /// generation of password reset tokens,
    /// email generation,
    /// some user helper methods to retrive user. 
    /// </summary>
    public sealed class UserManagerService : IUserManagerService,  IDisposable
    {

        private readonly UserManager<AppIdentityUser> _userMenager;
        private readonly SignInManager<AppIdentityUser, string> _signInMenager;
        private readonly RoleManager<IdentityRole> _roleManager;


        private readonly IAuthenticationManager _authenticationManager;

        public UserManagerService(UserManager<AppIdentityUser> userMenager, IAuthenticationManager authenticationManager, RoleManager<IdentityRole> roleManager)
        {
            this._userMenager = userMenager;
            this._authenticationManager = authenticationManager;
            this._roleManager = roleManager;

            //Pasword validation rules

            _userMenager.UserValidator = new UserValidator<AppIdentityUser>(userMenager) { RequireUniqueEmail = true, AllowOnlyAlphanumericUserNames = false };
            _userMenager.PasswordValidator = new PasswordValidator() { RequiredLength = 6, RequireLowercase = true, RequireUppercase = true, RequireDigit = true };
            _signInMenager = new SignInManager<AppIdentityUser, string>(_userMenager, _authenticationManager);

            //_userMenager.UserLockoutEnabledByDefault = true;
            //_userMenager.DefaultAccountLockoutTimeSpan = TimeSpan.FromDays(int.MaxValue);
            //_userMenager.SetLockoutEnabled(user.Id, enabled) // Enables or disables lockout for a user 
            //Register e-mail service for identity

            _userMenager.EmailService = new EmailService();

            //Token provider for password reset
            var dataProtectionProvider = Startup.dataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                IDataProtector dataProtector = dataProtectionProvider.Create("ASP.NET Identity");
                userMenager.UserTokenProvider = new DataProtectorTokenProvider<AppIdentityUser>(dataProtector);
            }
        }


        #region AppUserHelper
        public Task<AppIdentityUser> GetUserAsync(string name, string password)
        {
            return _userMenager.FindAsync(name, password);
        }

       public Task<AppIdentityUser> GetUserById(string id){

            return _userMenager.FindByIdAsync(id);
        }

        public Task<AppIdentityUser> GetUserByEmailAsync(string email)
        {
            return _userMenager.FindByEmailAsync(email);
        }

        public string GetUserName(string email)
        {
            AppIdentityUser user = _userMenager.FindByEmail(email);
            if (user != null) { return user.UserName; }
            return string.Empty;
        }
        #endregion

        #region AppSign
        public Task SignUserAsync(AppIdentityUser user, bool isPersistant)
        {
            return _signInMenager.SignInAsync(user, isPersistant, false);
        }

        public void SignOutUser()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
        #endregion

        #region AppUsers
        public Task<IdentityResult> CreateUserAsync(AppIdentityUser user, string password)
        {
            return _userMenager.CreateAsync(user, password);
        }
        public async Task<IdentityResult> UpdateUserData(AppIdentityUser user)
        {
            return await _userMenager.UpdateAsync(user);
        }
        public async Task<IdentityResult> RemoveUserData(AppIdentityUser user)
        {
            return await _userMenager.DeleteAsync(user);
        }
        public Task<ClaimsIdentity> CreateUserIdentityAsync(AppIdentityUser user, string type)
        {
            return _userMenager.CreateIdentityAsync(user, type);
        }

        public IList<AppIdentityUser> GetAllUsers()
        {
            return _userMenager.Users.ToList();
        }
        #endregion

        #region AppUsersRoles
        public Task<IdentityResult> AddUserToRoleAsync(AppIdentityUser user, string role)
        {
            return _userMenager.AddToRoleAsync(user.Id, role);
        }

        public Task<IdentityResult> RemoveUserFromRoleAsync(AppIdentityUser user, string role)
        {
            return _userMenager.RemoveFromRoleAsync(user.Id, role);
        }

        public IList<string> GetUserRole(string id)
        {
            return _userMenager.GetRoles(id);
        }
        #endregion

        #region Password
        public async Task<string> GeneratePasswordResetToken(string id)
        {
            var token = await _userMenager.GeneratePasswordResetTokenAsync(id);
            return token;
        }

        public async Task<IdentityResult> ResetUserPassword(string id, string token, string newPassword)
        {
            return await _userMenager.ResetPasswordAsync(id, token, newPassword);
        }

        public async Task SendResetPasswordMail(string callbackUrl, string Id)
        {
            await _userMenager.SendEmailAsync(Id, "Reset your password", "You will reset your password by clicking this link: " + callbackUrl + " ");
        }
        #endregion

        #region AppRoles
        public async Task<bool> CreateAppRoleAsync(IdentityRole role)
        {
            try
            {
                await _roleManager.CreateAsync(role);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        public async Task<bool> DeleteAppRoleAsync(IdentityRole role)
        {
            try
            {
                await _roleManager.DeleteAsync(role);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
          
        }

        public async Task<IdentityRole> FindAppRoleById(string id)
        {
            return await _roleManager.FindByIdAsync(id);
        }

        public async Task<bool> UpdateAppRoleAsync(IdentityRole role)
        {
            try
            {
                await _roleManager.UpdateAsync(role);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        #endregion


        public void Dispose()
        {
            _userMenager.Dispose();
            _signInMenager.Dispose();
        }

    }
}
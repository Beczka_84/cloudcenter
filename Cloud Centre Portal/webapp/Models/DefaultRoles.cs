﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models
{
    public static class DefaultRoles
    {
        public const string Admin = "Admin";
        public const string ServiceDesk = "Service Desk";
        public const string Partner = "Partner";
        public const string Customer = "Customer";
        public const string EndUser = "End User";
    }
}
﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models
{
    public class PresentationViewModel
    {

        public PresentationViewModel()
        {
            data = new List<HostedOrganisation>();
        }


        public bool showItemsForAdmin { get; set; }

        public bool showItemsForServiceDesk { get; set; }

        public bool showItemsForCustomer { get; set; }

        public bool showItemsForEndUser { get; set; }

        public bool showItemsForPartner { get; set; }


        public List<HostedOrganisation> data { get; set; }
    }
}
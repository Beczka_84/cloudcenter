﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Models.ViewModels
{
    public class ADUserViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int HostedOrganisationID { get; set; }
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Status")]
        public int StatusID { get; set; }
        public string SamAccountName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool SFBEnabled { get; set; }
        public bool DynamicsEnabled { get; set; }
        public bool SharePointEnabled { get; set; }
        public bool VDIEnabled { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SFBServicePlanID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? DynamicsServicePlanID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? SharePointServicePlanID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? VDIServicePlanID { get; set; }
        public bool IsEmailandApps { get; set; }
        public bool IsPrivateCloud { get; set; }
        public bool IsO365 { get; set; }
        public bool IsAzure { get; set; }

        public virtual ICollection<PhoneNumbersViewModel> PhoneNumbers { get; set; }
    }
}
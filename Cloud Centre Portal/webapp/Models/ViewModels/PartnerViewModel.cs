﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class PartnerViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? PriceListID { get; set; }
        public int? CallTariffID { get; set; }
        public int StatusID { get; set; }

        public string StatusName { get; set; }
        public string PriceListName { get; set; }
        public string CallTariffName { get; set; }

    }
}
﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class AppUsersViewModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> RolesId { get; set; }
        public List<string> RolesName { get; set; }

        public string Email { get; set; }
        public string UserName { get; set; }

        public int? HostedOrganisationsID { get; set; }
        public string HostedOrganisationName { get; set; }

        public string NewRole { get; set; }

    }
}
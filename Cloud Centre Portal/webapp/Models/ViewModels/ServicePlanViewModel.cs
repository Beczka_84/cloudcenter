﻿using CloudCentre.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class ServicePlanViewModel
    {
        public int ID { get; set; }
        [Required]
        [RemoteModel("IsNameUnique", "ServicePlan", "", AdditionalFields = "ID", HttpMethod = "GET", ErrorMessage = "Service plan already exists.")]
        public string Name { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }

    }
}
﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class PriceListViewModel
    {
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }

        public virtual ICollection<PriceListItemViewModel> PriceListItems { get; set; }
    }
}
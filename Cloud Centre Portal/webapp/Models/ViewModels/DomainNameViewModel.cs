﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class DomainNameViewModel
    {

        public int ID { get; set; }
        public string DomainName { get; set; }
        public int StatusID { get; set; }
        public int HostedOrganisationID { get; set; }
        public bool? ExchangeEnabled { get; set; }
        public bool? SFBEnabled { get; set; }

        public string StatusName { get; set; }
    }
}
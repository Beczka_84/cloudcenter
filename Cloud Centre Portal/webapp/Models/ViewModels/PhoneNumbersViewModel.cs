﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class PhoneNumbersViewModel
    {
        public string PhoneNumber { get; set; }
        public string DisplayNumber { get; set; }
        public int? HostedOrganisationID { get; set; }

        [Display(Name = "Status")]
        public int StatusID { get; set; }
    }
}
﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Models.ViewModels
{
    public class CallTariffItemViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int CallTariffID { get; set; }
        public string Destination { get; set; }
        public decimal? Cost { get; set; }
        public decimal PPM { get; set; }
    }
}
﻿using CloudCentre.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class StatusViewModel
    {
        public int Id { get; set; }

        [Required]
        [RemoteModel("IsNameUnique", "Status", "", AdditionalFields = "Id",  HttpMethod = "GET", ErrorMessage = "Status already exists.")]
        public string Text { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentre.Models.ViewModels
{
    public class HostedOrganisationViewModel
    {
        public int ID { get; set; }
        public int PartnerID { get; set; }
        public string Name { get; set; }
        public string OU { get; set; }
        public int StatusID { get; set; }
        public bool? DynamicsEnabled { get; set; }

        public string PartnerName { get; set; }
        public string StatusName { get; set; }

        public virtual ICollection<DomainNameViewModel> DomainNames { get; set; }
        public virtual ICollection<PhoneNumbersViewModel> PhoneNumbers { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;


namespace CloudCentre.Models.ViewModels
{
    public class AppRolesViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
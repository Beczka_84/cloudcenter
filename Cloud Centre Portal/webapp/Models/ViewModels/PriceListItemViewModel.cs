﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Models.ViewModels
{
    public class PriceListItemViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int ID { get; set; }

        public int StatusID { get; set; }
        public int ServicePlanID { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        public int PriceListID { get; set; }

        public string StatusName { get; set; }
        public string PriceListName { get; set; }
        public string ServicePlanName { get; set; }

    }
}
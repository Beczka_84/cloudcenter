﻿using CloudCentrePortal.DAL.Model;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CloudCentre.Models.ViewModels
{
    public class CallTariffViewModel
    {
        public CallTariffViewModel()
        {
            CallTariffItems = new List<CallTariffItemViewModel>();
        }

        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Display(Name = "Status")]
        public int StatusID { get; set; }

        public IList<CallTariffItemViewModel> CallTariffItems { get; set; }
    }
}
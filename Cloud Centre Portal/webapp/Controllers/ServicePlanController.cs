﻿using AutoMapper;
using CloudCentre.Attributes;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [StatusAction]
    public class ServicePlanController : Controller
    {
        private readonly IServicePlanService _servicePlanService;

        public ServicePlanController(IServicePlanService servicePlanService)
        {
            this._servicePlanService = servicePlanService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            List<ServicePlanViewModel> viewModelList = Mapper.Map<List<ServicePlanViewModel>>(_servicePlanService.GetServicePlans());
            return View(viewModelList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new ServicePlanViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ServicePlanViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            ServicePlan servicePlan = Mapper.Map<ServicePlan>(viewModel);
            ServiceResult result = _servicePlanService.AddServicePlan(servicePlan);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Service plan has been added";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Edit(int? servicePlanId)
        {
            if (servicePlanId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            int Id = Convert.ToInt32(servicePlanId);
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            var servicePlan = _servicePlanService.GetServicePlan(Id);
            if (servicePlan == null)
            {
                TempData["Error"] = "Error when editing service plan";
                return RedirectToAction("Index");
            }

            ServicePlanViewModel servicePlanViewModel = Mapper.Map<ServicePlanViewModel>(servicePlan);
            return View(servicePlanViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ServicePlanViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }

            ServicePlan servicePlan = Mapper.Map<ServicePlan>(viewModel);
            ServiceResult result = _servicePlanService.UpdateServicePlan(servicePlan);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Service plan has been edited";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult ConfirmDelete(int servicePlanId)
        {
            if (servicePlanId == 0 || servicePlanId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ServicePlan servicePlan = _servicePlanService.GetServicePlan(servicePlanId);

            if (servicePlan == null)
            {
                TempData["Error"] = "Error when deleting service plan";
                return RedirectToAction("Index");
            }

            ServicePlanViewModel servicePlanViewModel = Mapper.Map<ServicePlanViewModel>(servicePlan);
            return View(servicePlanViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ServicePlan servicePlan = _servicePlanService.GetServicePlan(Id);
            ServiceResult result = _servicePlanService.DeleteServicePlan(servicePlan);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"] = x.Value);
                return RedirectToAction("Index");
            }

            TempData["message"] = "service plan has been deleted";
            return RedirectToAction("Index");
        }

        public JsonResult IsNameUnique(string Name, int? Id = null)
        {
            return !_servicePlanService.CheckIfServicePlanExist(Name, Id) ? Json(true, JsonRequestBehavior.AllowGet) : Json(false, JsonRequestBehavior.AllowGet); ;
        }


    }
}
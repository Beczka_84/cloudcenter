﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using System.Net;
using CloudCentre.Attributes;
using CloudCentre.Models;

namespace CloudCentre.Controllers
{
    [StatusAction]
    [ADUserAction]
    public class AdUsersController : BaseController
    {
        private readonly IAdUserService _aDUsersService;

        public AdUsersController(IAdUserService adUserService)
        {
            this._aDUsersService = adUserService;
        }

        public ActionResult Index()
        {
            List<ADUserViewModel> viewModelList = Mapper.Map<List<ADUserViewModel>>(_aDUsersService.GetADUsers());
            return View(viewModelList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new ADUserViewModel());
        }

        public ActionResult AddPhoneNumber()
        {
            var phoneNumbersViewModel = new PhoneNumbersViewModel();
            return PartialView("_PhoneNumberPartial", phoneNumbersViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ADUserViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }


            ADUser adUser = Mapper.Map<ADUser>(viewModel);

            ServiceResult result = _aDUsersService.AddADUser(adUser);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "AD User has been added";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? Id)
        {
            if (Id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            int intId = Convert.ToInt32(Id);
            if (intId == 0 || intId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            ADUser adUser = _aDUsersService.GetADUser(intId);
            if (adUser == null)
            {
                TempData["Error"] = "Error when editing AD User";
                return RedirectToAction("Index");
            }

            ADUserViewModel AdUserViewModel = Mapper.Map<ADUserViewModel>(adUser);
            return View(AdUserViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ADUserViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }
            ADUser adUser = _aDUsersService.GetADUser(viewModel.ID);
            adUser.PhoneNumbers.ToList().ForEach(x => { adUser.PhoneNumbers.Remove(x); _aDUsersService.RemoveTelephone(x); });

            Mapper.Map(viewModel, adUser);

            ServiceResult result = _aDUsersService.UpdateADUser(adUser);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "AD User has been edited";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ConfirmDelete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ADUser adUser = _aDUsersService.GetADUser(Id);
            if (adUser == null)
            {
                TempData["Error"] = "Error when deleting ADUser";
                return RedirectToAction("Index");
            }

            ADUserViewModel aDUserViewModel = Mapper.Map<ADUserViewModel>(adUser);
            return View(aDUserViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ADUser aDUser = _aDUsersService.GetADUser(Id);
            aDUser.StatusID = (int)StatusEnum.Disabled;
            ServiceResult result = _aDUsersService.UpdateADUser(aDUser);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"] = x.Value);
                return RedirectToAction("Index");
            }
            TempData["message"] = "AD User has been deleted";
            return RedirectToAction("Index");
        }

    }
}
﻿using AutoMapper;
using CloudCentre.Attributes;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [PlanList]
    public class PriceListController : Controller
    {
        private readonly IPriceListService _priceListService;

        public PriceListController(IPriceListService priceListService)
        {
            this._priceListService = priceListService;
        }


        public ActionResult Index()
        {
            List<PriceListViewModel> viewModelList = Mapper.Map<List<PriceListViewModel>>(_priceListService.GetPriceLists());
            return View(viewModelList);
        }

        public ActionResult AddPriceListItem()
        {
            var priceListItemViewModel = new PriceListItemViewModel();
            return PartialView("_PriceListItemPartial", priceListItemViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new PriceListViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PriceListViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }


            PriceList priceList = Mapper.Map<PriceList>(viewModel);
            ServiceResult result = _priceListService.AddPriceList(priceList);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "price list has been added";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? Id)
        {
            if (Id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            int intId = Convert.ToInt32(Id);
            if (intId == 0 || intId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            PriceList priceList = _priceListService.GetPriceList(intId);
            if (priceList == null)
            {
                TempData["Error"] = "Error when editing price list";
                return RedirectToAction("Index");
            }

            PriceListViewModel priceListViewModel = Mapper.Map<PriceListViewModel>(priceList);
            return View(priceListViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PriceListViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }
            PriceList priceList = _priceListService.GetPriceList(viewModel.ID);

            priceList.PriceListItems.ToList().ForEach(x => { priceList.PriceListItems.Remove(x); _priceListService.RemovePriceListItem(x); });

            Mapper.Map(viewModel, priceList);
            ServiceResult result = _priceListService.UpdatePriceList(priceList);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Price List has been edited";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ConfirmDelete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            PriceList priceList = _priceListService.GetPriceList(Id);

            if (priceList == null)
            {
                TempData["Error"] = "Error when deleting price list";
                return RedirectToAction("Index");
            }

            PriceListViewModel priceListViewModel = Mapper.Map<PriceListViewModel>(priceList);
            return View(priceListViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            PriceList priceList = _priceListService.GetPriceList(Id);
            ServiceResult result = _priceListService.DeletePriceList(priceList);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"] = x.Value);
                return RedirectToAction("Index");
            }
            TempData["message"] = "price list has been deleted";
            return RedirectToAction("Index");
        }

    }
}
﻿using AutoMapper;
using CloudCentre.Attributes;
using CloudCentre.Models;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [Authorize(Roles = DefaultRoles.Admin)]
    [StatusAction]
    [RolesAction]
    public class AdminController : Controller
    {
        private readonly IUserManagerService _userMenagerService;
        private readonly IHostedOrganisationService _hostedOrganisationService;
        private readonly IRoleService _roleService;

        public AdminController(IUserManagerService userMenagerService, IRoleService roleService, IHostedOrganisationService hostedOrganisationService)
        {
            this._userMenagerService = userMenagerService;
            this._roleService = roleService;
            this._hostedOrganisationService = hostedOrganisationService;
        }


        public ActionResult GetUsers()
        {
            var list = _userMenagerService.GetAllUsers();
            IList<AppUsersViewModel> allUsers = Mapper.Map<IList<AppUsersViewModel>>(list);

            allUsers.ToList().ForEach(x => x.RolesName = _userMenagerService.GetUserRole(x.Id).ToList());
           
            return View(allUsers);
        }

        public async Task<ActionResult> EditUsers(string id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            AppIdentityUser user = await _userMenagerService.GetUserById(id);
            AppUsersViewModel userViewModel = Mapper.Map<AppUsersViewModel>(user);

            if (user == null || userViewModel == null)
            {
                ModelState.AddModelError("", "Error editing  user role");
                return View();
            }

            userViewModel.RolesName = _userMenagerService.GetUserRole(userViewModel.Id).ToList();
            return View(userViewModel);
        }


        public async Task<ActionResult> DeleteUser(string id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            AppIdentityUser user = await _userMenagerService.GetUserById(id);
            IdentityResult result = await _userMenagerService.RemoveUserData(user);

            if (!result.Succeeded)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError("", x.ToString()));
                return View("GetUsers");
            }

            return RedirectToAction("GetUsers");
        }

        [HttpPost]
        public async Task<ActionResult> EditUsers(AppUsersViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            AppIdentityUser orgUser = await _userMenagerService.GetUserById(viewModel.Id);

            if (orgUser == null)
            {
                ModelState.AddModelError("", "Error editing  user role");
                return View();
            }

            Mapper.Map(viewModel, orgUser);

            IdentityResult result = await _userMenagerService.UpdateUserData(orgUser);

            if (!result.Succeeded)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError("", x.ToString()));
                return View();
            }
            return RedirectToAction("GetUsers");
        }

        [HttpPost]
        public async Task<ActionResult> AddUserToRole(AppUsersViewModel viewModel)
        {
            if (viewModel == null || viewModel.Id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            AppIdentityUser user = await _userMenagerService.GetUserById(viewModel.Id);
            IdentityRole userRole = await _userMenagerService.FindAppRoleById(viewModel.NewRole);
            IdentityUserRole orgUserRole = user.Roles.FirstOrDefault();
           
            if (user == null || userRole == null)
            {
                ModelState.AddModelError("", "Error when adding new role");
                return View();
            }

            if(orgUserRole != null)
            {
                IdentityRole userOrgRole = await _userMenagerService.FindAppRoleById(orgUserRole.RoleId);
                IdentityResult resultRemove = await _userMenagerService.RemoveUserFromRoleAsync(user, userOrgRole.Name);

                    if (!resultRemove.Succeeded)
                    {
                        resultRemove.Errors.ToList().ForEach(x => ModelState.AddModelError("", x.ToString()));
                        return View();
                    }
            }

            IdentityResult result = await _userMenagerService.AddUserToRoleAsync(user, userRole.Name);
            if (!result.Succeeded)
            {
                result.Errors.ToList().ForEach(x=>ModelState.AddModelError("", x.ToString()));
                return View();
            }

            TempData["message"] = "User has been added to role";
            return RedirectToAction("GetUsers");
        }

        public async Task<ActionResult> RemoveUserFromRole(string roleId, string userId)
        {
            if (roleId == null || userId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            AppIdentityUser orgUser = await _userMenagerService.GetUserById(userId);
            IdentityRole userRole = await _userMenagerService.FindAppRoleById(roleId);

            if (orgUser == null || userRole == null) {
                ModelState.AddModelError("", "Error when removing role");
                return View();
            }

            IdentityResult result =  await  _userMenagerService.RemoveUserFromRoleAsync(orgUser, userRole.Name);

            if (!result.Succeeded)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError("", x.ToString()));
                return View();
            }

            TempData["message"] = "User role has been deleted";
            return RedirectToAction("GetUsers");
        }


        
        public ActionResult GetRoles()
        {
            var list = _roleService.GetAppRoles();
            IEnumerable<AppRolesViewModel> allRoles = Mapper.Map<IEnumerable<AppRolesViewModel>>(list);
            return View(allRoles);
        }

        public ActionResult AddRoles()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddRoles(AppRolesViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }

            IdentityRole role = Mapper.Map<IdentityRole>(viewModel);
            var result = await _userMenagerService.CreateAppRoleAsync(role);

            if (!result) {
                ModelState.AddModelError("", "Error when adding new role");
                return View(viewModel);
            }
            TempData["message"] = "Role has been added";
            return RedirectToAction("GetRoles");
        }

        public async Task<ActionResult> EditRoles(string id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            IdentityRole role = await _userMenagerService.FindAppRoleById(id);

            AppRolesViewModel viewModel = Mapper.Map<AppRolesViewModel>(role);
            return View(viewModel);
        }


        [HttpPost]
        public async Task<ActionResult> EditRoles(AppRolesViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }

            IdentityRole role = await _userMenagerService.FindAppRoleById(viewModel.Id);
            Mapper.Map(viewModel, role);

            if (role == null)
            {
                ModelState.AddModelError("", "Error when editing role");
                return View(viewModel);
            }

            var result = await _userMenagerService.UpdateAppRoleAsync(role);

            if (!result)
            {
                ModelState.AddModelError("", "Error when editing role");
                return View(viewModel);
            }
            TempData["message"] = "Role has been edited";
            return RedirectToAction("GetRoles");
        }


        public async Task<ActionResult> DeleteRoles(string id)
        {

            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            IdentityRole role = await _userMenagerService.FindAppRoleById(id);

            if(role == null) {
                ModelState.AddModelError("", "Error when deleting role");
                return View();
            }

            bool result =  await _userMenagerService.DeleteAppRoleAsync(role);
            if (!result)
            {
                ModelState.AddModelError("", "Error when deleting role");
                return View();
            }

            TempData["message"] = "Role has been deleted";
            return RedirectToAction("GetRoles");
        }

    }
}
﻿using AutoMapper;
using CloudCentre.Attributes;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [StatusAction]
    public class CallTarifController : BaseController
    {
        private readonly ICallTarifService _callTarifService;
        private readonly IStatusService _statusService;

        public CallTarifController(ICallTarifService callTarifService, IStatusService statusService)
        {
            this._callTarifService = callTarifService;
            this._statusService = statusService;
        }

        public ActionResult Index()
        {
            List<CallTariffViewModel> viewModelList = Mapper.Map<List<CallTariffViewModel>>(_callTarifService.GetCallTariffs());
            return View(viewModelList);
        }

        public ActionResult AddCallTarifItem()
        {
            var callTariffItemViewModel = new CallTariffItemViewModel();
            return PartialView("_CallTariffItemPartial", callTariffItemViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new CallTariffViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CallTariffViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }


            CallTariff callTariff = Mapper.Map<CallTariff>(viewModel);
            ServiceResult result = _callTarifService.AddCallTariff(callTariff);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Call Tarifs has been added";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? Id)
        {
            if (Id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            int intId = Convert.ToInt32(Id);
            if (intId == 0 || intId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            CallTariff callTariff = _callTarifService.GetCallTariff(intId);
            if (callTariff == null)
            {
                TempData["Error"] = "Error when editing status";
                return RedirectToAction("Index");
            }

            CallTariffViewModel CallTariffViewModel = Mapper.Map<CallTariffViewModel>(callTariff);
            return View(CallTariffViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CallTariffViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }
            CallTariff callTariff = _callTarifService.GetCallTariff(viewModel.ID);

            //http://stackoverflow.com/questions/5538974/the-relationship-could-not-be-changed-because-one-or-more-of-the-foreign-key-pro
            callTariff.CallTariffItems.ToList().ForEach(x => { callTariff.CallTariffItems.Remove(x); _callTarifService.RemoveCallTariff(x); });

            Mapper.Map(viewModel, callTariff);
            ServiceResult result = _callTarifService.UpdateCallTariff(callTariff);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Status has been edited";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult ConfirmDelete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            CallTariff callTariff = _callTarifService.GetCallTariff(Id);

            if (callTariff == null)
            {
                TempData["Error"] = "Error when deleting status";
                return RedirectToAction("Index");
            }

            CallTariffViewModel callTariffViewModel = Mapper.Map<CallTariffViewModel>(callTariff);
            return View(callTariffViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            CallTariff callTariff = _callTarifService.GetCallTariff(Id);
            ServiceResult result = _callTarifService.DeleteCallTariff(callTariff);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"] = x.Value);
                return RedirectToAction("Index");
            }
            TempData["message"] = "Status has been deleted";
            return RedirectToAction("Index");
        }
    }
}
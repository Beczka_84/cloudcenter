﻿using AutoMapper;
using CloudCentre.Attributes;
using CloudCentre.Models;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [HostedOrganisation]
    public class HostedOrganisationController : Controller
    {
        private readonly IHostedOrganisationService _hostedOrganisationService;
        private readonly IUserManagerService _userService;

        public HostedOrganisationController(IHostedOrganisationService hostedOrganisationService, IUserManagerService userService)
        {
            this._hostedOrganisationService = hostedOrganisationService;
            this._userService = userService;
        }

        public ActionResult Index()
        {

            string userId = User.Identity.GetUserId();
            string role = _userService.GetUserRole(userId).FirstOrDefault();

            List<HostedOrganisationViewModel> viewModelList = Mapper.Map<List<HostedOrganisationViewModel>>(_hostedOrganisationService.GetHostedOrganisations(userId, role));
            return View(viewModelList);
        }

        public ActionResult AddDomainName()
        {
            var domainNameViewModelViewModel = new DomainNameViewModel();
            return PartialView("_DomainNamesPartial", domainNameViewModelViewModel);
        }

        public ActionResult AddPhoneNumber()
        {
            var phoneNumbersViewModel = new PhoneNumbersViewModel();
            return PartialView("_PhoneNumberPartial", phoneNumbersViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new HostedOrganisationViewModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HostedOrganisationViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }


            HostedOrganisation hostedOrganisation = Mapper.Map<HostedOrganisation>(viewModel);
            ServiceResult result = _hostedOrganisationService.AddHostedOrganisation(hostedOrganisation);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Hosted Organisation has been added";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? Id)
        {
            if (Id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            int intId = Convert.ToInt32(Id);
            if (intId == 0 || intId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            HostedOrganisation hostedOrganisation = _hostedOrganisationService.GetHostedOrganisation(intId);
            if (hostedOrganisation == null)
            {
                TempData["Error"] = "Error when editing Hosted Organisation";
                return RedirectToAction("Index");
            }

            HostedOrganisationViewModel hostedOrganisationViewModel = Mapper.Map<HostedOrganisationViewModel>(hostedOrganisation);
            return View(hostedOrganisationViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HostedOrganisationViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }
            HostedOrganisation hostedOrganisation = _hostedOrganisationService.GetHostedOrganisation(viewModel.ID);
            hostedOrganisation.DomainNames.ToList().ForEach(x => { hostedOrganisation.DomainNames.Remove(x); _hostedOrganisationService.RemoveDomainNames(x); });
            hostedOrganisation.PhoneNumbers.ToList().ForEach(x => { hostedOrganisation.PhoneNumbers.Remove(x); _hostedOrganisationService.RemoveTelephone(x);});

            Mapper.Map(viewModel, hostedOrganisation);

            ServiceResult result = _hostedOrganisationService.UpdateHostedOrganisation(hostedOrganisation);
            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Hosted Organisation has been edited";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult ConfirmDelete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            HostedOrganisation hostedOrganisation = _hostedOrganisationService.GetHostedOrganisation(Id);
            if (hostedOrganisation == null)
            {
                TempData["Error"] = "Error when deleting Hosted Organisation";
                return RedirectToAction("Index");
            }

            HostedOrganisationViewModel hostedOrganisationViewModel = Mapper.Map<HostedOrganisationViewModel>(hostedOrganisation);
            return View(hostedOrganisationViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            HostedOrganisation hostedOrganisation = _hostedOrganisationService.GetHostedOrganisation(Id);
            hostedOrganisation.StatusID = (int)StatusEnum.Disabled;
            ServiceResult result = _hostedOrganisationService.UpdateHostedOrganisation(hostedOrganisation);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"] = x.Value);
                return RedirectToAction("Index");
            }
            TempData["message"] = "Hosted Organisation has been deleted";
            return RedirectToAction("Index");
        }


    }
}
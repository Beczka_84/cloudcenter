﻿#region Using

using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using CloudCentrePortal.Models;
using CloudCentre.Services.Abstract;
using AutoMapper;
using CloudCentre.Models;
using CloudCentre.Controllers;
using CloudCentre.Attributes;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net;

#endregion

namespace CloudCentrePortal.Controllers
{
    [Account]
    public class AccountController : BaseController
    {
        private readonly IUserManagerService _userService;
        private readonly IHostedOrganisationService _hostedOrganisationService;

        public AccountController(IUserManagerService userService, IHostedOrganisationService hostedOrganisationService)
        {
            this._userService = userService;
            this._hostedOrganisationService = hostedOrganisationService;
        }

        public async Task<ActionResult> ForgotPassword(string userId)
        {
            if (userId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            var user =  await _userService.GetUserById(userId);
            if (user == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            AccountForgotPasswordModel viewModel = Mapper.Map<AccountForgotPasswordModel>(user);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(AccountForgotPasswordModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }
            var user = await _userService.GetUserByEmailAsync(viewModel.Email);
            if (user==null) {
                ModelState.AddModelError("", "There is no such user");
                return View(viewModel);
            }

            var token = await _userService.GeneratePasswordResetToken(user.Id);
            await _userService.SendResetPasswordMail(Url.Action("ConfirmPasswordResset", "Account", new { passToken = token, userId = user.Id }, protocol: Request.Url.Scheme), user.Id);

            TempData["message"] = string.Format("Password reset email has been send");
            return View();
        }

        public ActionResult ConfirmPasswordResset(string passToken, string userId)
        {
            if (passToken == null || userId == null)
            {
                return View("Error");
            }
            ConfirmPasswordResetViewModel model = new ConfirmPasswordResetViewModel() { Token = passToken, Id = userId };
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfirmPasswordResset(ConfirmPasswordResetViewModel viewModel)
        {
            if (!ModelState.IsValid){return View(viewModel);}
            IdentityResult result = await _userService.ResetUserPassword(viewModel.Id, viewModel.Token, viewModel.NewPassword);

            if (result.Succeeded) { return RedirectToAction("Login"); }
             
            result.Errors.ToList().ForEach(x => ModelState.AddModelError("", x));
            return View(viewModel);
        }


        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View(new AccountLoginModel() { ReturnUrl = returnUrl});
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountLoginModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            var user = await _userService.GetUserAsync(_userService.GetUserName(viewModel.Email), viewModel.Password);
            if (user != null)
                {
                try
                    {
                        await _userService.SignUserAsync(user, viewModel.RememberMe);
                        return RedirectToLocal(viewModel.ReturnUrl);
                    }
                catch (Exception)
                    {
                        ModelState.AddModelError("", "user can't be signed");
                        return View(viewModel);
                    }
            }
            ModelState.AddModelError("", "Invalid email or password");
            return View(viewModel);
        }

        
        public ActionResult Logout()
        {
            _userService.SignOutUser();
            return RedirectToAction("Login");
        }


        public ActionResult Register(int Id = 0)
        {
            //if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            AccountRegistrationModel viewModel = new AccountRegistrationModel();
            if (Id != 0)
            {
                viewModel.HostedOrganisationsID = Id;
                viewModel.HostedOrganisationsName = _hostedOrganisationService.GetHostedOrganisation(Id).Name;
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AccountRegistrationModel viewModel)
        {
            if (!ModelState.IsValid) {
                return View(viewModel);
            };

            AppIdentityUser user = Mapper.Map<AppIdentityUser>(viewModel);
            user.BirthDate = DateTime.Now;
            try
            {
                var result = await _userService.CreateUserAsync(user, viewModel.Password);
                IdentityRole role = await _userService.FindAppRoleById(viewModel.RoleID);

                var roleresult = await _userService.AddUserToRoleAsync(user, role.Name);   
                if (!result.Succeeded || !roleresult.Succeeded)
                {
                    result.Errors.ToList().ForEach(a => ModelState.AddModelError("", a.ToString()));
                    return View(viewModel);
                }

                await _userService.CreateUserIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToLocal();
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("", ex.InnerException.ToString());
                return View(viewModel);
            }
        }


        [AllowAnonymous]
        public ActionResult Lock()
        {
            return View();
        }


        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            if (!returnUrl.IsNullOrWhiteSpace() && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("index", "HostedOrganisation");
        }


       
    }
}
﻿using AutoMapper;
using CloudCentre.Attributes;
using CloudCentre.Models;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    [Partner]
    public class PartnerController : Controller
    {

        private readonly IPartnerService _partnerService;
        private readonly IUserManagerService _userService;

        public PartnerController(IPartnerService partnerService, IUserManagerService userService)
        {
            this._partnerService = partnerService;
            this._userService = userService;
        }


        public ActionResult Index()
        {

            string userId = User.Identity.GetUserId();
            string role = _userService.GetUserRole(userId).FirstOrDefault();

            List<PartnerViewModel> viewModelList = Mapper.Map<List<PartnerViewModel>>(_partnerService.GetPartners(userId, role));
            return View(viewModelList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new PartnerViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PartnerViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            Partner partner = Mapper.Map<Partner>(viewModel);
            ServiceResult result = _partnerService.AddPartner(partner);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Partner has been added";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? partnerId)
        {
            if (partnerId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            int Id = Convert.ToInt32(partnerId);
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            var partner = _partnerService.GetPartner(Id);
            if (partner == null)
            {
                TempData["Error"] = "Error when editing partner";
                return RedirectToAction("Index");
            }

            PartnerViewModel partnerViewModel = Mapper.Map<PartnerViewModel>(partner);
            return View(partnerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PartnerViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }

            Partner partner = Mapper.Map<Partner>(viewModel);

            ServiceResult result = _partnerService.UpdatePartner(partner);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Partner has been edited";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult ConfirmDelete(int partnerId)
        {
            if (partnerId == 0 || partnerId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            Partner partner = _partnerService.GetPartner(partnerId);

            if (partner == null)
            {
                TempData["Error"] = "Error when deleting partner";
                return RedirectToAction("Index");
            }

            PartnerViewModel viewModel = Mapper.Map<PartnerViewModel>(partner);
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            Partner partner = _partnerService.GetPartner(Id);
            partner.StatusID = (int)StatusEnum.Disabled;
            ServiceResult result = _partnerService.UpdatePartner(partner);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"] = x.Value);
                return RedirectToAction("Index");
            }

            TempData["message"] = "Partner has been set as Disabled";
            return RedirectToAction("Index");
        }

    }
}
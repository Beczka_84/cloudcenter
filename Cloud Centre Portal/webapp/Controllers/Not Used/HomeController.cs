﻿#region Using

using CloudCentre.Models;
using CloudCentrePortal.DAL;
using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    public class HomeController : Controller
    {
       
        // GET: home/index
        public ActionResult Index()
        {
            bool check = User.IsInRole(DefaultRoles.EndUser);

            return View();
        }

        // GET: home/organisations
        public ActionResult Organisations()
        {
            return View();
        }

        // GET: home/calendar
        public ActionResult Calendar()
        {
            return View();
        }

        // GET: home/google-map
        public ActionResult GoogleMap()
        {
            return View();
        }
    }
}
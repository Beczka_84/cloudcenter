﻿#region Using

using System.Linq;
using System.Web.Mvc;


#endregion

namespace CloudCentrePortal.Controllers
{
    public class OrganisationsController : Controller
    {

        // GET: Organisations/Partners
        public ActionResult Partners()
        {
            //var partners = new CloudCentre.Models.CCSEEntities();
            //partners.Partners.ToList()
            return View();
        }

        // GET: Organisations/HostedOrganisations

        public ActionResult HostedOrganisations()
        {
            
            return View();
        }

        // GET: Organisations/DomainNames
        public ActionResult DomainNames()
        {
            return View();
        }

        // GET: Organisations/Users
        public ActionResult Users()
        {
            return View();
        }
    }
}
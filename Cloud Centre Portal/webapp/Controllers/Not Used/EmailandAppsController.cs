﻿#region Using

using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    [Authorize]
    public class EmailandAppsController : Controller
    {
        // GET: EmailandApps/MicrosoftExchange
        public ActionResult MicrosoftExchange()
        {
            return View();
        }

        // GET: EmailandApps/SkypeforBusiness
        public ActionResult SkypeforBusiness()
        {
            return View();
        }

        // GET: EmailandApps/MicrosoftSharePoint
        public ActionResult MicrosoftSharePoint()
        {
            return View();
        }

        // GET: EmailandApps/MicrosoftDynamics
        public ActionResult MicrosoftDynamics()
        {
            return View();
        }
    }
}
﻿#region Using

using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    public class SettingsController : Controller
    {
        // GET: /Settings/Call Tariffs
        public ActionResult CallTariffs()
        {
            return View();
        }

        // GET: /Settings/Price Lists
        public ActionResult PriceLists()
        {
            return View();
        }

        // GET: /Settings/Data Corrections
        public ActionResult DataCorrections()
        {
            return View();
        }
        
    }
}
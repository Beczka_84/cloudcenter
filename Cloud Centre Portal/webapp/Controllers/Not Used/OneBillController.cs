﻿#region Using

using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    public class OneBillController : Controller
    {
        // GET: /OneBill/Partner Summary/
        public ActionResult PartnerSummary()
        {
            return View();
        }

        // GET: /OneBill/Account Charges/
        public ActionResult AccountCharges()
        {
            return View();
        }

        // GET: /OneBill/Invoices/
        public ActionResult Invoices()
        {
            return View();
        }
    }
}
﻿using CloudCentre.Models;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    public class PresentationController : Controller
    {
        private readonly AppContext _context;
        private readonly IUserManagerService _userMenagerService;
        public PresentationController(AppContext context, IUserManagerService userMenagerService)
        {
           this._context = context;
           this._userMenagerService = userMenagerService;
        }

        public async Task<ActionResult> Index()
        {
            PresentationViewModel presentationViewModel = new PresentationViewModel();

            presentationViewModel.showItemsForAdmin = User.IsInRole(DefaultRoles.Admin);
            presentationViewModel.showItemsForServiceDesk = User.IsInRole(DefaultRoles.ServiceDesk);
            presentationViewModel.showItemsForCustomer = User.IsInRole(DefaultRoles.Customer);
            presentationViewModel.showItemsForPartner = User.IsInRole(DefaultRoles.Partner);
            presentationViewModel.showItemsForEndUser = User.IsInRole(DefaultRoles.EndUser);

            if (User.IsInRole(DefaultRoles.Admin))
            {
                presentationViewModel.data = _context.HostedOrganisations.ToList(); //all organizations
            }

            //Just an example - now we will have access to Hosted Organisations of current user
            //having Hosted Organisations Id we could refer to partners
            if (User.IsInRole(DefaultRoles.Partner))
            {
                string currenUserId = User.Identity.GetUserId();
                var user = await _userMenagerService.GetUserById(currenUserId);     //Only user organizations
                presentationViewModel.data.Add(user.HostedOrganisations);
            }

            return View(presentationViewModel);
        }
    }
}
﻿#region Using

using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    public class ReportsController : Controller
    {
        // GET: /Reports/My Calls/
        public ActionResult MyCalls()
        {
            return View();
        }

        // GET: /Reports/Call Reports/
        public ActionResult CallReports()
        {
            return View();
        }

        // GET: /Reports/Service Summary/
        public ActionResult ServiceSummary()
        {
            return View();
        }

        
    }
}
﻿#region Using

using System.Web.Mvc;

#endregion

namespace CloudCentrePortal.Controllers
{
    public class PhoneNumbersController : Controller
    {
        // GET: PhoneNumbers/UnassignedNumbers
        public ActionResult UnassignedNumbers()
        {
            return View();
        }

        // GET: PhoneNumbers/AssignedNumbers
        public ActionResult AssignedNumbers()
        {
            return View();
        }

    }
}
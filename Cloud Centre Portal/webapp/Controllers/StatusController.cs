﻿using AutoMapper;
using CloudCentre.Models.ViewModels;
using CloudCentre.Services;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Controllers
{
    public class StatusController : Controller
    {
        private readonly IStatusService _statusService;

        public StatusController(IStatusService statusService)
        {
            this._statusService = statusService;
        }

        public ActionResult Index()
        {
            List<StatusViewModel> viewModelList = Mapper.Map<List<StatusViewModel>>(_statusService.GetStatuses());
            return View(viewModelList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new StatusViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StatusViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            Status status = Mapper.Map<Status>(viewModel);
            ServiceResult result = _statusService.AddStatus(status);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Status has been added";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? StatusId)
        {
            if (StatusId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            int Id = Convert.ToInt32(StatusId);
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }

            var status = _statusService.GetStatus(Id);
            if (status == null)
            {
                TempData["Error"] = "Error when editing status";
                return RedirectToAction("Index");
            }

            StatusViewModel statusViewModel = Mapper.Map<StatusViewModel>(status);
            return View(statusViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StatusViewModel viewModel)
        {
            if (viewModel == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (!ModelState.IsValid) { return View(viewModel); }

            Status status = Mapper.Map<Status>(viewModel);

            ServiceResult result = _statusService.UpdateStatus(status);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => ModelState.AddModelError(x.Key.ToString(), x.Value));
                return View();
            }

            TempData["message"] = "Status has been edited";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult ConfirmDelete(int StatusId)
        {
            if (StatusId == 0 || StatusId < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            Status status =  _statusService.GetStatus(StatusId);

            if (status == null)
            {
                TempData["Error"] = "Error when deleting status";
                return RedirectToAction("Index");
            }

            StatusViewModel viewModel = Mapper.Map<StatusViewModel>(status);
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int Id)
        {
            if (Id == 0 || Id < 0) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            Status status = _statusService.GetStatus(Id);
            ServiceResult result = _statusService.DeleteStatus(status);

            if (!result.IsValid)
            {
                result.Errors.ToList().ForEach(x => TempData["Error"]= x.Value);
                return RedirectToAction("Index");
            }

            TempData["message"] = "Status has been deleted";
            return RedirectToAction("Index");
        }


        public JsonResult IsNameUnique(string Text, int? Id = null)
        {
            return !_statusService.CheckIfStatusExist(Text, Id) ? Json(true, JsonRequestBehavior.AllowGet) : Json(false, JsonRequestBehavior.AllowGet); ;
        }


    }
}
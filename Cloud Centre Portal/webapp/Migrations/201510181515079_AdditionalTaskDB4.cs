namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionalTaskDB4 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.PriceListItems", "ServicePlanID");
            AddForeignKey("dbo.PriceListItems", "ServicePlanID", "dbo.ServicePlans", "ID", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PriceListItems", "ServicePlanID", "dbo.ServicePlans");
            DropIndex("dbo.PriceListItems", new[] { "ServicePlanID" });
        }
    }
}

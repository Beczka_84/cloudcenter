namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionalTaskDB5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "HostedOrganisationsID", "dbo.HostedOrganisations");
            AddForeignKey("dbo.Users", "HostedOrganisationsID", "dbo.HostedOrganisations", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "HostedOrganisationsID", "dbo.HostedOrganisations");
            AddForeignKey("dbo.Users", "HostedOrganisationsID", "dbo.HostedOrganisations", "ID", cascadeDelete: false);
        }
    }
}

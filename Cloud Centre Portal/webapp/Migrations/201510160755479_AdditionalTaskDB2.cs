namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionalTaskDB2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Status", "Text", c => c.String());
            CreateIndex("dbo.Partners", "PriceListID");
            CreateIndex("dbo.Partners", "CallTariffID");
            AddForeignKey("dbo.Partners", "CallTariffID", "dbo.CallTariffs", "ID");
            AddForeignKey("dbo.Partners", "PriceListID", "dbo.PriceLists", "ID");
            DropColumn("dbo.Status", "Status1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Status", "Status1", c => c.String());
            DropForeignKey("dbo.Partners", "PriceListID", "dbo.PriceLists");
            DropForeignKey("dbo.Partners", "CallTariffID", "dbo.CallTariffs");
            DropIndex("dbo.Partners", new[] { "CallTariffID" });
            DropIndex("dbo.Partners", new[] { "PriceListID" });
            DropColumn("dbo.Status", "Text");
        }
    }
}

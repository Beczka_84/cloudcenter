namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TypoInHosted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HostedOrganisations", "DynamicsEnabled", c => c.Boolean());
            DropColumn("dbo.HostedOrganisations", "DyanicsEnabled");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HostedOrganisations", "DyanicsEnabled", c => c.Boolean());
            DropColumn("dbo.HostedOrganisations", "DynamicsEnabled");
        }
    }
}

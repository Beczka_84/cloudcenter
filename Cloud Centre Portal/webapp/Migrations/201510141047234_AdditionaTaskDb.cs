namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdditionaTaskDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ADUsers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        HostedOrganisationID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        SamAccountName = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmailAddress = c.String(),
                        SFBEnabled = c.Boolean(nullable: false),
                        DynamicsEnabled = c.Boolean(nullable: false),
                        SharePointEnabled = c.Boolean(nullable: false),
                        VDIEnabled = c.Boolean(nullable: false),
                        SFBServicePlanID = c.Int(),
                        DynamicsServicePlanID = c.Int(),
                        SharePointServicePlanID = c.Int(),
                        VDIServicePlanID = c.Int(),
                        IsEmailandApps = c.Boolean(nullable: false),
                        IsPrivateCloud = c.Boolean(nullable: false),
                        IsO365 = c.Boolean(nullable: false),
                        IsAzure = c.Boolean(nullable: false),
                        ServicePlans_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HostedOrganisations", t => t.HostedOrganisationID, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: false)
                .ForeignKey("dbo.ServicePlans", t => t.ServicePlans_ID)
                .Index(t => t.HostedOrganisationID)
                .Index(t => t.StatusID)
                .Index(t => t.ServicePlans_ID);
            
            CreateTable(
                "dbo.DomainNames",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DomainName = c.String(),
                        StatusID = c.Int(nullable: false),
                        HostedOrganisationID = c.Int(nullable: false),
                        ExchangeEnabled = c.Boolean(),
                        SFBEnabled = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.HostedOrganisations", t => t.HostedOrganisationID, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: false)
                .Index(t => t.StatusID)
                .Index(t => t.HostedOrganisationID);
            
            CreateTable(
                "dbo.CallTariffs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: false)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.CallTariffItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Destination = c.String(),
                        Cost = c.Decimal(precision: 18, scale: 2),
                        PPM = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CallTariffID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CallTariffs", t => t.CallTariffID, cascadeDelete: true)
                .Index(t => t.CallTariffID);
            
            CreateTable(
                "dbo.PhoneNumbers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(),
                        DisplayNumber = c.String(),
                        HostedOrganisationID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        UserID = c.Int(),
                        ADUsers_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ADUsers", t => t.ADUsers_ID)
                .ForeignKey("dbo.HostedOrganisations", t => t.HostedOrganisationID, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: false)
                .Index(t => t.HostedOrganisationID)
                .Index(t => t.StatusID)
                .Index(t => t.ADUsers_ID);
            
            CreateTable(
                "dbo.PriceLists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: true)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.PriceListItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StatusID = c.Int(nullable: false),
                        ServicePlanID = c.Int(nullable: false),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PriceListID = c.Int(nullable: false),
                        Status_Id = c.Int(),
                        Status_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.PriceLists", t => t.PriceListID, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: false)
                .ForeignKey("dbo.Status", t => t.Status_Id)
                .ForeignKey("dbo.Status", t => t.Status_Id1)
                .Index(t => t.StatusID)
                .Index(t => t.PriceListID)
                .Index(t => t.Status_Id)
                .Index(t => t.Status_Id1);
            
            CreateTable(
                "dbo.ServicePlans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: true)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.HostedOrganisations", "DyanicsEnabled", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServicePlans", "StatusID", "dbo.Status");
            DropForeignKey("dbo.ADUsers", "ServicePlans_ID", "dbo.ServicePlans");
            DropForeignKey("dbo.PriceListItems", "Status_Id1", "dbo.Status");
            DropForeignKey("dbo.PriceListItems", "Status_Id", "dbo.Status");
            DropForeignKey("dbo.PriceLists", "StatusID", "dbo.Status");
            DropForeignKey("dbo.PriceListItems", "StatusID", "dbo.Status");
            DropForeignKey("dbo.PriceListItems", "PriceListID", "dbo.PriceLists");
            DropForeignKey("dbo.PhoneNumbers", "StatusID", "dbo.Status");
            DropForeignKey("dbo.PhoneNumbers", "HostedOrganisationID", "dbo.HostedOrganisations");
            DropForeignKey("dbo.PhoneNumbers", "ADUsers_ID", "dbo.ADUsers");
            DropForeignKey("dbo.DomainNames", "StatusID", "dbo.Status");
            DropForeignKey("dbo.CallTariffs", "StatusID", "dbo.Status");
            DropForeignKey("dbo.CallTariffItems", "CallTariffID", "dbo.CallTariffs");
            DropForeignKey("dbo.ADUsers", "StatusID", "dbo.Status");
            DropForeignKey("dbo.DomainNames", "HostedOrganisationID", "dbo.HostedOrganisations");
            DropForeignKey("dbo.ADUsers", "HostedOrganisationID", "dbo.HostedOrganisations");
            DropIndex("dbo.ServicePlans", new[] { "StatusID" });
            DropIndex("dbo.PriceListItems", new[] { "Status_Id1" });
            DropIndex("dbo.PriceListItems", new[] { "Status_Id" });
            DropIndex("dbo.PriceListItems", new[] { "PriceListID" });
            DropIndex("dbo.PriceListItems", new[] { "StatusID" });
            DropIndex("dbo.PriceLists", new[] { "StatusID" });
            DropIndex("dbo.PhoneNumbers", new[] { "ADUsers_ID" });
            DropIndex("dbo.PhoneNumbers", new[] { "StatusID" });
            DropIndex("dbo.PhoneNumbers", new[] { "HostedOrganisationID" });
            DropIndex("dbo.CallTariffItems", new[] { "CallTariffID" });
            DropIndex("dbo.CallTariffs", new[] { "StatusID" });
            DropIndex("dbo.DomainNames", new[] { "HostedOrganisationID" });
            DropIndex("dbo.DomainNames", new[] { "StatusID" });
            DropIndex("dbo.ADUsers", new[] { "ServicePlans_ID" });
            DropIndex("dbo.ADUsers", new[] { "StatusID" });
            DropIndex("dbo.ADUsers", new[] { "HostedOrganisationID" });
            DropColumn("dbo.HostedOrganisations", "DyanicsEnabled");
            DropTable("dbo.Vendors");
            DropTable("dbo.ServicePlans");
            DropTable("dbo.PriceListItems");
            DropTable("dbo.PriceLists");
            DropTable("dbo.PhoneNumbers");
            DropTable("dbo.CallTariffItems");
            DropTable("dbo.CallTariffs");
            DropTable("dbo.DomainNames");
            DropTable("dbo.ADUsers");
        }
    }
}

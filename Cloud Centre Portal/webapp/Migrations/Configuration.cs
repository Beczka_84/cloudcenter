namespace CloudCentre.Migrations
{
    using Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using CloudCentrePortal.DAL.Model;
    using System.Collections.Generic;
    using Microsoft.AspNet.Identity;

    internal sealed class Configuration : DbMigrationsConfiguration<CloudCentrePortal.DAL.AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CloudCentrePortal.DAL.AppContext context)
        {
            var store = new UserStore<AppIdentityUser>(context);
            var manager = new UserManager<AppIdentityUser>(store);

            Status statusEnabled = new Status() { Id = (int)StatusEnum.Enabled, Text = "Enabled" };
            Status statusDisabled = new Status() { Id = (int)StatusEnum.Disabled, Text = "Disabled" };

            context.Status.AddOrUpdate(statusEnabled);
            context.Status.AddOrUpdate(statusDisabled);

            var storeRole = new RoleStore<IdentityRole>(context);
            var managerRole = new RoleManager<IdentityRole>(storeRole);

            var role1 = new IdentityRole { Name = DefaultRoles.Admin };
            var role2 = new IdentityRole { Name = DefaultRoles.Customer };
            var role3 = new IdentityRole { Name = DefaultRoles.EndUser };
            var role4 = new IdentityRole { Name = DefaultRoles.Partner };
            var role5 = new IdentityRole { Name = DefaultRoles.ServiceDesk };

            managerRole.Create(role1);
            managerRole.Create(role2);
            managerRole.Create(role3);
            managerRole.Create(role4);
            managerRole.Create(role5);

            var passwordHash = new PasswordHasher();
            string password = passwordHash.HashPassword("Password@123");

            var user1 = new AppIdentityUser()
            {
                UserName = "Admin",
                Email = "lukasz_oz@interia.pl",
                PasswordHash = password,
                FirstName = "someFirstName1",
                LastName = "someLastName1",
                BirthDate = DateTime.Now,
                SecurityStamp = "a"
            };

            manager.Create(user1);

            //var user2 = new AppIdentityUser()
            //{
            //    UserName = "userToTest2",
            //    Email = "some2@email.com",
            //    PasswordHash = password,
            //    FirstName = "someFirstName2",
            //    LastName = "someLastName2",
            //    BirthDate = DateTime.Now,
            //    SecurityStamp = "b"
            //};

            //var user3 = new AppIdentityUser()
            //{
            //    UserName = "userToTest3",
            //    Email = "some3@email.com",
            //    PasswordHash = password,
            //    FirstName = "someFirstName3",
            //    LastName = "someLastName3",
            //    BirthDate = DateTime.Now,
            //    SecurityStamp = "c"
            //};


            //var hostedOrganisations = new List<HostedOrganisation>()
            //{

            //    new HostedOrganisation() { Name = "test", OU = "test", Status = new Status() {Status1 = "Enabled" },
            //                               Partner = new Partner() { Name = "some Partner1", Status = new Status() { Status1 = "Enabled"} },
            //                               AppUsers = new List<AppIdentityUser> () {user1}},

            //    new HostedOrganisation() { Name = "test2", OU = "test2", Status = new Status() { Status1 = "Disabled" },
            //                               Partner = new Partner() { Name = "some Partner2" , Status = new Status() { Status1 = "Disabled"} },
            //                               AppUsers = new List<AppIdentityUser>() { user2, user3 }}
            //};


            //hostedOrganisations.ForEach(x => context.HostedOrganisations.AddOrUpdate(x));
            context.SaveChanges();
            manager.AddToRole(user1.Id, DefaultRoles.Admin);
            //manager.AddToRole(user2.Id, DefaultRoles.Partner);
            //manager.AddToRole(user3.Id, DefaultRoles.EndUser);

        }
    }
}

namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HOPhoneNumber : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PhoneNumbers", "HostedOrganisationID", "dbo.HostedOrganisations");
            DropIndex("dbo.PhoneNumbers", new[] { "HostedOrganisationID" });
            AlterColumn("dbo.PhoneNumbers", "HostedOrganisationID", c => c.Int());
            CreateIndex("dbo.PhoneNumbers", "HostedOrganisationID");
            AddForeignKey("dbo.PhoneNumbers", "HostedOrganisationID", "dbo.HostedOrganisations", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PhoneNumbers", "HostedOrganisationID", "dbo.HostedOrganisations");
            DropIndex("dbo.PhoneNumbers", new[] { "HostedOrganisationID" });
            AlterColumn("dbo.PhoneNumbers", "HostedOrganisationID", c => c.Int(nullable: false));
            CreateIndex("dbo.PhoneNumbers", "HostedOrganisationID");
            AddForeignKey("dbo.PhoneNumbers", "HostedOrganisationID", "dbo.HostedOrganisations", "ID", cascadeDelete: true);
        }
    }
}

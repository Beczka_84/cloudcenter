// <auto-generated />
namespace CloudCentre.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AdditionaTaskDb : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AdditionaTaskDb));
        
        string IMigrationMetadata.Id
        {
            get { return "201510141047234_AdditionaTaskDb"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HostedOrganisation",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PartnerID = c.Int(nullable: false),
                        Name = c.String(),
                        OU = c.String(),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Partner", t => t.PartnerID, cascadeDelete: true)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: false)
                .Index(t => t.PartnerID)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.Partner",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PriceListID = c.Int(),
                        CallTariffID = c.Int(),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Status", t => t.StatusID, cascadeDelete: true)
                .Index(t => t.StatusID);
            
            CreateTable(
                "dbo.Status",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Status1 = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Partner", "StatusID", "dbo.Status");
            DropForeignKey("dbo.HostedOrganisation", "StatusID", "dbo.Status");
            DropForeignKey("dbo.HostedOrganisation", "PartnerID", "dbo.Partner");
            DropIndex("dbo.Partner", new[] { "StatusID" });
            DropIndex("dbo.HostedOrganisation", new[] { "StatusID" });
            DropIndex("dbo.HostedOrganisation", new[] { "PartnerID" });
            DropTable("dbo.Status");
            DropTable("dbo.Partner");
            DropTable("dbo.HostedOrganisation");
        }
    }
}

namespace CloudCentre.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HostedOrganizations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "HostedOrganisationsID", c => c.Int());
            CreateIndex("dbo.Users", "HostedOrganisationsID");
            AddForeignKey("dbo.Users", "HostedOrganisationsID", "dbo.HostedOrganisations", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "HostedOrganisationsID", "dbo.HostedOrganisations");
            DropIndex("dbo.Users", new[] { "HostedOrganisationsID" });
            DropColumn("dbo.Users", "HostedOrganisationsID");
        }
    }
}

﻿$('#addTarifItem').click(function () {
    $.ajax({
        async: false,
        url: '/CallTarif/AddCallTarifItem'
    }).success(function (partialView) {
        $('#next-TarifItem').append(partialView);
    });
});
$("body").on("click", "a.deleteRow", function () {
    $(this).parents(".TariffItemRow").remove();
    return false;
});


$('#addPriceListItem').click(function () {
    $.ajax({
        async: false,
        url: '/PriceList/AddPriceListItem'
    }).success(function (partialView) {
        $('#next-PriceListItem').append(partialView);
    });
});
$("body").on("click", "a.deletePriceListItemRow", function () {
    $(this).parents(".PriceListItemRow").remove();
    return false;
});


$('#addDomainName').click(function () {
    $.ajax({
        async: false,
        url: '/HostedOrganisation/AddDomainName'
    }).success(function (partialView) {
        $('#next-DomainName').append(partialView);
    });
});

$("body").on("click", "a.deleteDomainRow", function () {
    $(this).parents(".DomainNameRow").remove();
    return false;
});

$('#addPhoneNumber').click(function () {
    $.ajax({
        async: false,
        url: '/HostedOrganisation/AddPhoneNumber'
    }).success(function (partialView) {
        $('#next-PhoneNumber').append(partialView);
    });
});

$("body").on("click", "a.deletePhoneNumber", function () {
    $(this).parents(".PhoneNameRow").remove();
    return false;
});

using CloudCentre.Services.Abstract;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CloudCentre.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CloudCentre.App_Start.NinjectWebCommon), "Stop")]

namespace CloudCentre.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject.Extensions.Conventions;
    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Mvc;

    using CloudCentrePortal.DAL;
    using Services.Concrete;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.Owin.Security;
    using Models;
    using CloudCentrePortal.DAL.Model;
    using DAL.UnitOfWork.Concrete;
    using DAL.UnitOfWork.Abstract;
    using Ninject.Web.Mvc.FilterBindingSyntax;
    using Attributes;

    /// <summary>
    /// DI class used to inject 
    /// </summary>
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                //kernel.

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            

            kernel.Bind<AppContext>().ToSelf().InRequestScope();
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();

            kernel.BindFilter<StatusActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<StatusActionAttribute>();
            kernel.BindFilter<RolesActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<RolesActionAttribute>();
            kernel.BindFilter<PlanListActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<PlanListAttribute>();
            kernel.BindFilter<PartnerActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<PartnerAttribute>();
            kernel.BindFilter<HostedOrganisationActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<HostedOrganisationAttribute>();
            kernel.BindFilter<AccountActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<AccountAttribute>();
            kernel.BindFilter<ADUserActionFilter>(System.Web.Mvc.FilterScope.Controller, 0).WhenControllerHas<ADUserActionAttribute>();


            

            kernel.Bind(x => { x.From(typeof(UserManagerService).Assembly).SelectAllClasses().EndingWith("Service").BindDefaultInterface(); });
            kernel.Bind(typeof (IAdUserService)).To(typeof (AdUserSevice));

            kernel.Bind(typeof(IUserStore<AppIdentityUser>)).To(typeof(UserStore<AppIdentityUser>)).WithConstructorArgument("context", kernel.Get<AppContext>());
            kernel.Bind(typeof(UserManager<AppIdentityUser>)).To(typeof(UserManager<AppIdentityUser>)).WithConstructorArgument("store", kernel.Get<IUserStore<AppIdentityUser>>());

            kernel.Bind(typeof(IRoleStore<IdentityRole, string>)).To(typeof(RoleStore<IdentityRole>)).WithConstructorArgument("context", kernel.Get<AppContext>());
            kernel.Bind(typeof(RoleManager<IdentityRole>)).To(typeof(RoleManager<IdentityRole>)).WithConstructorArgument("store", kernel.Get<IRoleStore<IdentityRole, string>>());

            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication);
        }        
    }
}

﻿using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(CloudCentre.App_Start.Startup))]

namespace CloudCentre.App_Start
{

    /// <summary>
    /// Owin Startup class - all Identity 2.0 config goes here like cookies config etc..
    /// </summary>
    public class Startup
    {
        internal static IDataProtectionProvider dataProtectionProvider { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            dataProtectionProvider = app.GetDataProtectionProvider();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                CookieName = "CloudCentreApp",
                //ExpireTimeSpan = System.TimeSpan.FromMinutes(10)
            });
        }
    }
}

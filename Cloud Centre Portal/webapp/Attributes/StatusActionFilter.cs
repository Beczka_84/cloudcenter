﻿using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Attributes
{

    public class StatusActionAttribute : FilterAttribute { }

    public class StatusActionFilter : ActionFilterAttribute
    {
        private readonly IStatusService _statusService;

        public StatusActionFilter(IStatusService statusService)
        {
            this._statusService = statusService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.AllStatuses = new SelectList(_statusService.GetStatuses(), "Id", "Text").ToList();
            base.OnActionExecuting(filterContext);
        }
    }
}
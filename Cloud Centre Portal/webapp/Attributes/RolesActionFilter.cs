﻿using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Attributes
{

    public class RolesActionAttribute : FilterAttribute { }

    public class RolesActionFilter : ActionFilterAttribute
    {

        private readonly IRoleService _roleService;

        public RolesActionFilter(IRoleService roleService)
        {
            this._roleService = roleService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.AllRoles = new SelectList(_roleService.GetAppRoles(), "Id", "Name").ToList();
            base.OnActionExecuting(filterContext);
        }
    }
}
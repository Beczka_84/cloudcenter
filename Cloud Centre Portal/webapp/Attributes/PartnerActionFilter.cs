﻿using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Attributes
{

    public class PartnerAttribute : FilterAttribute { }


    public class PartnerActionFilter : ActionFilterAttribute
    {
        private readonly IStatusService _statusService;
        private readonly ICallTarifService _callTarifService;
        private readonly IPriceListService _priceListService;

        public PartnerActionFilter(IStatusService statusService, ICallTarifService callTarifService, IPriceListService priceListService)
        {
            this._statusService = statusService;
            this._callTarifService = callTarifService;
            this._priceListService = priceListService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.AllStatuses = new SelectList(_statusService.GetStatuses(), "Id", "Text").ToList();
            filterContext.Controller.ViewBag.AllPriceList = new SelectList(_priceListService.GetPriceLists(), "ID", "Name").ToList();
            filterContext.Controller.ViewBag.AllCallTariffs = new SelectList(_callTarifService.GetCallTariffs(), "ID", "Name").ToList();

            base.OnActionExecuting(filterContext);
        }
    }
}
﻿using System.Web.Mvc;

namespace CloudCentre.Attributes
{
    public class RemoteModelAttribute : RemoteAttribute
    {
        public RemoteModelAttribute(string action, string controller, string area) : base(action, controller, area)
        {
        }
    }
}
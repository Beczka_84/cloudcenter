﻿using CloudCentre.Models;
using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity;
using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Attributes
{

    public class AccountAttribute : FilterAttribute { }


    /// <summary>
    /// Class used as a attribute in Account Controleer to generate data for views dropdows
    /// </summary>
    public class AccountActionFilter : ActionFilterAttribute
    {
        private readonly IHostedOrganisationService _hostedOrganisationService;
        private readonly IRoleService _roleService;
        private readonly IUserManagerService _userManagerService;

        public AccountActionFilter(IHostedOrganisationService hostedOrganisationService, IRoleService roleService, IUserManagerService userManagerService)
        {
            this._hostedOrganisationService = hostedOrganisationService;
            this._roleService = roleService;
            this._userManagerService = userManagerService;
        }



        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string userId  =  filterContext.HttpContext.User.Identity.GetUserId();
            string role = _userManagerService.GetUserRole(userId).FirstOrDefault();


            filterContext.Controller.ViewBag.Genders = PrepareLookUps();
            filterContext.Controller.ViewBag.AllHostedOrganisations = new SelectList(_hostedOrganisationService.GetHostedOrganisations(userId, role), "ID", "Name").ToList();
            filterContext.Controller.ViewBag.AllRoles = new SelectList(_roleService.GetAppRoles(), "Id", "Name").ToList();

            base.OnActionExecuting(filterContext);
        }

        public List<SelectListItem> PrepareLookUps()
        {
            List<SelectListItem> AvailableGenders = new List<SelectListItem>() {
                new SelectListItem() { Text = "Gender", Disabled=true, Selected=true, Value="1"},
                new SelectListItem() { Text = "Male" , Value="2"},
                new SelectListItem() { Text = "Female" , Value="3"},
                new SelectListItem() { Text = "Prefer not to answer", Value="4"},
            };

            return AvailableGenders;

        }
    }
}
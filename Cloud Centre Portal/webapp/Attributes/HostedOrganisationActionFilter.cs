﻿using CloudCentre.Services.Abstract;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity;
using Ninject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CloudCentre.Attributes
{

    public class HostedOrganisationAttribute : FilterAttribute { }


    public class HostedOrganisationActionFilter : ActionFilterAttribute
    {
        private readonly IStatusService _statusService;
        private readonly IPartnerService _partnerService;
        private readonly IUserManagerService _userService;

        public HostedOrganisationActionFilter(IStatusService statusService, IPartnerService partnerService, IUserManagerService userService)
        {
            this._statusService = statusService;
            this._partnerService = partnerService;
            this._userService = userService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            string userId = filterContext.HttpContext.User.Identity.GetUserId();
            string role = _userService.GetUserRole(userId).FirstOrDefault();

            filterContext.Controller.ViewBag.AllStatuses = new SelectList(_statusService.GetStatuses(), "Id", "Text").ToList();

            filterContext.Controller.ViewBag.AllPartners = new SelectList(_partnerService.GetPartners(userId, role), "ID", "Name").ToList();
            base.OnActionExecuting(filterContext);
        }
    }
}
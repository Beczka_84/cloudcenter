﻿using CloudCentre.Services.Abstract;
using Ninject;
using System.Linq;
using System.Web.Mvc;

namespace CloudCentre.Attributes
{
    public class PlanListAttribute : FilterAttribute { }

    public class PlanListActionFilter : ActionFilterAttribute
    {

        private readonly IStatusService _statusService;
        private readonly IServicePlanService _servicePlanService;

        public PlanListActionFilter(IStatusService statusService, IServicePlanService servicePlanService)
        {
            this._statusService = statusService;
            this._servicePlanService = servicePlanService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.AllStatuses = new SelectList(_statusService.GetStatuses(), "Id", "Text").ToList();
            filterContext.Controller.ViewBag.AllServicePlans = new SelectList(_servicePlanService.GetServicePlans(), "ID", "Name").ToList();

            base.OnActionExecuting(filterContext);
        }
    }
}
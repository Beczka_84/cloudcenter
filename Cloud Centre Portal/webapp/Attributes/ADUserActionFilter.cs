﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CloudCentre.Services.Abstract;
using Microsoft.AspNet.Identity;

namespace CloudCentre.Attributes
{

    public class ADUserActionAttribute : FilterAttribute { }

    public class ADUserActionFilter : ActionFilterAttribute
    {
            private readonly IHostedOrganisationService _HostedOrganisationService;
            private readonly IServicePlanService _ServicePlanService;
            private readonly IUserManagerService _userService;


        public ADUserActionFilter(IHostedOrganisationService hostedOrganisationService, IServicePlanService servicePlanService, IUserManagerService userService)
            {
                this._HostedOrganisationService = hostedOrganisationService;
                this._ServicePlanService = servicePlanService;
                this._userService = userService;
            }

            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                string userId = filterContext.HttpContext.User.Identity.GetUserId();
                string role = _userService.GetUserRole(userId).FirstOrDefault();

                filterContext.Controller.ViewBag.AllHostedOrg = new SelectList(_HostedOrganisationService.GetHostedOrganisations(userId, role), "ID", "Name").ToList();
                filterContext.Controller.ViewBag.AllServicePlans = new SelectList(_ServicePlanService.GetServicePlans(), "ID", "Name").ToList();
                base.OnActionExecuting(filterContext);
            }
    }
}
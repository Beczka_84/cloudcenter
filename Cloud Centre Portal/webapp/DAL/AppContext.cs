﻿using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace CloudCentrePortal.DAL
{
    public class AppContext : IdentityDbContext<IdentityUser> 
    {
        public AppContext() : base("AppContext")
        {
        }

        public virtual DbSet<ADUser> AdUsers { get; set; }
        public virtual DbSet<CallTariff> CallTariffs { get; set; }
        public virtual DbSet<CallTariffItem> CallTariffItems { get; set; }
        public virtual DbSet<DomainNames> DomainNames { get; set; }
        public virtual DbSet<PhoneNumbers> PhoneNumbers { get; set; }
        public virtual DbSet<PriceList> PriceLists { get; set; }
        public virtual DbSet<PriceListItem> PriceListItems { get; set; }
        public virtual DbSet<ServicePlan> ServicePlans { get; set; }
        public virtual DbSet<Vendor> Vendors { get; set; }


        public virtual DbSet<HostedOrganisation> HostedOrganisations { get; set; }
        public virtual DbSet<Partner> Partners { get; set; }
        public virtual DbSet<Status> Status { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityUser>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
        }
    }
}

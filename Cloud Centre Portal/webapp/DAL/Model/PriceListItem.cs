﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentrePortal.DAL.Model
{
    public class PriceListItem
    {
        public int ID { get; set; }
        public int StatusID { get; set; }
        public int ServicePlanID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int PriceListID { get; set; }

        public virtual PriceList PriceList { get; set; }
        public virtual ServicePlan ServicePlan { get; set; }
        public virtual Status Status { get; set; }
    }
}
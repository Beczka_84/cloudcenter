﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CloudCentrePortal.DAL.Model
{
    public class ADUser
    {
      
        public int ID { get; set; }

        public int HostedOrganisationID { get; set; }

        public int StatusID { get; set; }
        public string SamAccountName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public bool SFBEnabled { get; set; }
        public bool DynamicsEnabled { get; set; }
        public bool SharePointEnabled { get; set; }
        public bool VDIEnabled { get; set; }
        public int? SFBServicePlanID { get; set; }
        public int? DynamicsServicePlanID { get; set; }
        public int? SharePointServicePlanID { get; set; }
        public int? VDIServicePlanID { get; set; }
        public bool IsEmailandApps { get; set; }
        public bool IsPrivateCloud { get; set; }
        public bool IsO365 { get; set; }
        public bool IsAzure { get; set; }

        public virtual HostedOrganisation HostedOrganisations { get; set; }
        public virtual ServicePlan ServicePlans { get; set; }
        public virtual ICollection<PhoneNumbers> PhoneNumbers { get; set; }
    }
}
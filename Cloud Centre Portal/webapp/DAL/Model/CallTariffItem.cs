﻿namespace CloudCentrePortal.DAL.Model
{
    public class CallTariffItem
    {
        public int ID { get; set; }
        public string Destination { get; set; }
        public decimal? Cost { get; set; }
        public decimal PPM { get; set; }
        public int CallTariffID { get; set; }

        public virtual CallTariff CallTariffs { get; set; }
    }
}
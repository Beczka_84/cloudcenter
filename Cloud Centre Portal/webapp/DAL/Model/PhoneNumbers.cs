﻿namespace CloudCentrePortal.DAL.Model
{
    public class PhoneNumbers
    {
        public int ID { get; set; }
        public string PhoneNumber { get; set; }
        public string DisplayNumber { get; set; }
        public int? HostedOrganisationID { get; set; }
        public int StatusID { get; set; }
        public int? UserID { get; set; }

        public virtual ADUser ADUsers { get; set; }
        public virtual HostedOrganisation HostedOrganisations { get; set; }
        public virtual Status Status { get; set; }
    }
}
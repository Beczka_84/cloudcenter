﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentrePortal.DAL.Model
{
    public class Status
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual ICollection<HostedOrganisation> HostedOrganisations { get; set; }
        public virtual ICollection<Partner> Partners { get; set; }
        public virtual ICollection<ADUser> ADUsers { get; set; }
        public virtual ICollection<CallTariff> CallTariffs { get; set; }
        public virtual ICollection<DomainNames> DomainNames { get; set; }
        public virtual ICollection<PhoneNumbers> PhoneNumbers { get; set; }
        public virtual ICollection<PriceList> PriceList { get; set; }
        public virtual ICollection<PriceListItem> PriceListItems { get; set; }
        public virtual ICollection<PriceListItem> PriceListItems1 { get; set; }
        public virtual ICollection<ServicePlan> ServicePlans { get; set; }
    }
}

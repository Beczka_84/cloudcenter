﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentrePortal.DAL.Model
{
    public class Partner
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? PriceListID { get; set; }
        public int? CallTariffID { get; set; }
        public int StatusID { get; set; }

        public virtual ICollection<HostedOrganisation> HostedOrganisations { get; set; }
        public virtual Status Status { get; set; }
        public virtual PriceList PriceList { get; set; }
        public virtual CallTariff CallTariffs { get; set; }
    }
}

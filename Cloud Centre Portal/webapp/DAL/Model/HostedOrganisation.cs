﻿using System.Collections.Generic;

namespace CloudCentrePortal.DAL.Model
{
        public class HostedOrganisation
        {
            public int ID { get; set; }
            public int PartnerID { get; set; }
            public string Name { get; set; }
            public string OU { get; set; }
            public int StatusID { get; set; }
            public bool? DynamicsEnabled { get; set; }


            public virtual ICollection<ADUser> ADUsers { get; set; }
            public virtual ICollection<DomainNames> DomainNames { get; set; }
            public virtual ICollection<PhoneNumbers> PhoneNumbers { get; set; }
            public virtual Partner Partner { get; set; }
            public virtual Status Status { get; set; }
            public virtual ICollection<AppIdentityUser> AppUsers { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace CloudCentrePortal.DAL.Model
{
    public class PriceList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StatusID { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<PriceListItem> PriceListItems { get; set; }
        public virtual ICollection<Partner> Partners { get; set; }
    }
}
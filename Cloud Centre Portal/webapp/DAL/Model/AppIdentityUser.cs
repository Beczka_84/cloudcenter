﻿using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CloudCentrePortal.DAL.Model
{
    public class AppIdentityUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }

        public int? HostedOrganisationsID { get; set; }
        public virtual HostedOrganisation HostedOrganisations { get; set; }

    }
}
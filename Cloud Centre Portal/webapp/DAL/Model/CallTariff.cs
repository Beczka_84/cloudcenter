﻿using CloudCentrePortal.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace CloudCentrePortal.DAL.Model
{
    public class CallTariff
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StatusID { get; set; }

        public virtual ICollection<CallTariffItem> CallTariffItems { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Partner> Partners { get; set; }
    }
}
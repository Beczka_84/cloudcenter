﻿
namespace CloudCentrePortal.DAL.Model
{
    public class DomainNames
    {
        public int ID { get; set; }
        public string DomainName { get; set; }
        public int StatusID { get; set; }
        public int HostedOrganisationID { get; set; }
        public bool? ExchangeEnabled { get; set; }
        public bool? SFBEnabled { get; set; }

        public virtual HostedOrganisation HostedOrganisations { get; set; }
        public virtual Status Status { get; set; }
    }
}
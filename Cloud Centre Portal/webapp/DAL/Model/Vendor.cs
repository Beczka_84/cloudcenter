﻿namespace CloudCentrePortal.DAL.Model
{
    public class Vendor
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
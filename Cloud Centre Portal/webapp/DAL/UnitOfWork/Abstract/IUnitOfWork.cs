﻿using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudCentre.DAL.UnitOfWork.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        GenericRepository<HostedOrganisation> HostedOrganisations { get; }
        GenericRepository<ADUser> ADUser { get; }
        GenericRepository<CallTariff> CallTariff { get; }
        GenericRepository<CallTariffItem> CallTariffItems { get; }
        GenericRepository<DomainNames> DomainNames { get; }
        GenericRepository<PhoneNumbers> PhoneNumbers { get; }
        GenericRepository<PriceList> PriceList { get; }
        GenericRepository<PriceListItem> PriceListItem { get; }
        GenericRepository<ServicePlan> ServicePlan { get; }
        GenericRepository<Vendor> Vendor { get; }
        GenericRepository<Partner> Partner { get; }
        GenericRepository<Status> Status { get; }
        GenericRepository<IdentityRole> AppRoles { get; }
        void Save();
    }
}

﻿using CloudCentre.DAL.UnitOfWork.Abstract;
using CloudCentrePortal.DAL;
using CloudCentrePortal.DAL.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace CloudCentre.DAL.UnitOfWork.Concrete
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly AppContext _context;

        private readonly GenericRepository<HostedOrganisation> _HostedOrganisations;
        private readonly GenericRepository<ADUser> _ADUser;
        private readonly GenericRepository<CallTariff> _CallTariff;
        private readonly GenericRepository<CallTariffItem> _CallTariffItems;
        private readonly GenericRepository<DomainNames> _DomainNames;
        private readonly GenericRepository<PhoneNumbers> _PhoneNumbers;
        private readonly GenericRepository<PriceList> _PriceList;
        private readonly GenericRepository<PriceListItem> _PriceListItem;
        private readonly GenericRepository<ServicePlan> _ServicePlan;
        private readonly GenericRepository<Vendor> _Vendor;
        private readonly GenericRepository<Partner> _Partner;
        private readonly GenericRepository<Status> _Status;

        private readonly GenericRepository<IdentityRole> _IdentityRole;


        public UnitOfWork(AppContext context)
        {
            this._context = context;

            this._ADUser = new GenericRepository<ADUser>(_context);
            this._CallTariff = new GenericRepository<CallTariff>(_context);
            this._CallTariffItems =  new GenericRepository<CallTariffItem>(_context);
            this._DomainNames =  new GenericRepository<DomainNames>(_context);
            this._PhoneNumbers = new GenericRepository<PhoneNumbers>(_context);
            this._PriceList =  new GenericRepository<PriceList>(_context);
            this._PriceListItem = new GenericRepository<PriceListItem>(_context);
            this._ServicePlan =  new GenericRepository<ServicePlan>(_context);
            this._Vendor =  new GenericRepository<Vendor>(_context);
            this._Partner =  new GenericRepository<Partner>(_context);
            this._Status = new GenericRepository<Status>(_context);
            this._HostedOrganisations =new GenericRepository<HostedOrganisation>(_context);
            this._IdentityRole = new GenericRepository<IdentityRole>(_context);
        }


        public GenericRepository<IdentityRole> AppRoles
        {
            get
            {
                return this._IdentityRole;
            }

        }


        public GenericRepository<HostedOrganisation> HostedOrganisations
        {
            get 
            {
                return this._HostedOrganisations;
            }

        }

        public GenericRepository<ADUser> ADUser
        {
            get
            {
                return this._ADUser;
            }

        }

        public GenericRepository<CallTariff> CallTariff
        {
            get
            {
                return this._CallTariff;
            }

        }

        public GenericRepository<CallTariffItem> CallTariffItems
        {
            get
            {
                return this._CallTariffItems;
            }

        }

        public GenericRepository<DomainNames> DomainNames
        {
            get
            {
                return this._DomainNames;
            }

        }

        public GenericRepository<PhoneNumbers> PhoneNumbers
        {
            get
            {
                return this._PhoneNumbers;
            }

        }

        public GenericRepository<PriceList> PriceList
        {
            get
            {
                return this._PriceList;
            }

        }

        public GenericRepository<PriceListItem> PriceListItem
        {
            get
            {
                return this._PriceListItem;
            }

        }

        public GenericRepository<ServicePlan> ServicePlan
        {
            get
            {
                return this._ServicePlan;
            }

        }

        public GenericRepository<Vendor> Vendor
        {
            get
            {
                return this._Vendor;
            }

        }

        public GenericRepository<Partner> Partner
        {
            get
            {
                return this._Partner;
            }

        }

        public GenericRepository<Status> Status
        {
            get
            {
                return this._Status;
            }

        }

        //Save to db one place for services except identity 2.0 
        public void Save()
        {
            _context.SaveChanges();
        }


        private bool disposed = false;
        protected virtual void Dispose(bool disposing = false)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this._context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}
﻿using AutoMapper;

namespace CloudCentre.Infrastructure.MapperConfig
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<CloudCentreMappings>();
            });
        }
    }
}
﻿using AutoMapper;
using CloudCentre.Models;
using System.Linq;
using CloudCentre.Models.ViewModels;
using CloudCentrePortal.DAL.Model;
using CloudCentrePortal.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace CloudCentre.Infrastructure.MapperConfig
{
    public class CloudCentreMappings : Profile
    {
        public override string ProfileName
        {
            get { return "CloudCentreMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<AccountRegistrationModel, AppIdentityUser>();

            Mapper.CreateMap<AppRolesViewModel, IdentityRole>()
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.Users, opt => opt.Ignore());

            Mapper.CreateMap<IdentityRole, AppRolesViewModel>()
                .ForSourceMember(x => x.Users, opt => opt.Ignore());

            Mapper.CreateMap<AppIdentityUser, AppUsersViewModel>()
                .ForMember(x => x.RolesId, opt => opt.ResolveUsing<CustomResolver>())
                .ForMember(x => x.HostedOrganisationName, opt => opt.MapFrom(y => y.HostedOrganisations.Name))
                .ForMember(x => x.HostedOrganisationsID, opt => opt.MapFrom(y => y.HostedOrganisationsID));

            var map = Mapper.CreateMap<AppUsersViewModel, AppIdentityUser>();
            map.ForAllMembers(x => x.Ignore());
            map.ForMember(x => x.FirstName, opt => opt.MapFrom(y => y.FirstName));
            map.ForMember(x => x.LastName, opt => opt.MapFrom(y => y.LastName));
            map.ForMember(x => x.UserName, opt => opt.MapFrom(y => y.UserName));

            //
            Mapper.CreateMap<AppIdentityUser, AccountForgotPasswordModel>();
            Mapper.CreateMap<AccountForgotPasswordModel, AppIdentityUser>();

            //Status mappings
            Mapper.CreateMap<StatusViewModel, Status>();
            Mapper.CreateMap<Status, StatusViewModel>();


            Mapper.CreateMap<Status, CallTariffViewModel>();
            //Mapper.CreateMap<Status, Status>();

            Mapper.CreateMap<CallTariffViewModel, CallTariff>()
                .ForMember(dest => dest.Status, opts => opts.Ignore())
                .ForMember(dest => dest.StatusID, opts => opts.MapFrom(x => x.StatusID));

            Mapper.CreateMap<CallTariff, CallTariffViewModel>();


            Mapper.CreateMap<CallTariffItemViewModel, CallTariffItem>()
                .ForMember(dest => dest.CallTariffs, opts => opts.Ignore());



            Mapper.CreateMap<CallTariffItem, CallTariffItemViewModel>();


            //Service Plan mappings
            Mapper.CreateMap<ServicePlanViewModel, ServicePlan>();
            Mapper.CreateMap<ServicePlan, ServicePlanViewModel>()
                .ForMember(dest => dest.StatusName, opts => opts.MapFrom(x => x.Status.Text));


            //Price List Item mappings
            Mapper.CreateMap<PriceListViewModel, PriceList>();
            Mapper.CreateMap<PriceList, PriceListViewModel>()
                .ForMember(dest => dest.StatusName, opts => opts.MapFrom(x => x.Status.Text));

            Mapper.CreateMap<PriceListItemViewModel, PriceListItem>();
            Mapper.CreateMap<PriceListItem, PriceListItemViewModel>()
                .ForMember(dest => dest.StatusName, opts => opts.MapFrom(x => x.Status.Text))
                .ForMember(dest => dest.PriceListName, opts => opts.MapFrom(x => x.PriceList.Name))
                .ForMember(dest => dest.ServicePlanName, opts => opts.MapFrom(x => x.ServicePlan.Name));

            //Partner mappings
            Mapper.CreateMap<PartnerViewModel, Partner>();
            Mapper.CreateMap<Partner, PartnerViewModel>()
                .ForMember(dest => dest.StatusName, opts => opts.MapFrom(x => x.Status.Text))
                .ForMember(dest => dest.PriceListName, opts => opts.MapFrom(x => x.PriceList.Name))
                .ForMember(dest => dest.CallTariffName, opts => opts.MapFrom(x => x.CallTariffs.Name));


            //HostedOrganisation mappings
            Mapper.CreateMap<HostedOrganisationViewModel, HostedOrganisation>();
            Mapper.CreateMap<HostedOrganisation, HostedOrganisationViewModel>()
                .ForMember(dest => dest.StatusName, opts => opts.MapFrom(x => x.Status.Text))
                .ForMember(dest => dest.PartnerName, opts => opts.MapFrom(x => x.Partner.Name));

            //Domain Names mappings
            Mapper.CreateMap<DomainNameViewModel, DomainNames>();
            Mapper.CreateMap<DomainNames, DomainNameViewModel>()
                .ForMember(dest => dest.StatusName, opts => opts.MapFrom(x => x.Status.Text));

            //Phone Numbers mappings
            Mapper.CreateMap<PhoneNumbersViewModel, PhoneNumbers>();
            Mapper.CreateMap<PhoneNumbers, PhoneNumbersViewModel>();

            //Phone Ad User mappings
            Mapper.CreateMap<ADUser, ADUserViewModel>();
            Mapper.CreateMap<ADUserViewModel, ADUser>();



        }

        public class CustomResolver : ValueResolver<AppIdentityUser, List<string>>
        {
            protected override List<string> ResolveCore(AppIdentityUser source)
            {
                return source.Roles.Select(x => x.RoleId).ToList();
            }
        }


    }

}
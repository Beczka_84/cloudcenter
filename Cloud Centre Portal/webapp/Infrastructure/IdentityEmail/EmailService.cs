﻿using Microsoft.AspNet.Identity;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CloudCentre.Infrastructure.IdentityEmail
{
        /// <summary>
        /// Email service used to genereate email for identity 2.0 you could change this to Postal for example.
        /// </summary>
        public class EmailService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {

                var mailMessage = new MailMessage(
                "", 
                message.Destination,
                message.Subject,
                message.Body
                );

                SmtpClient client = new SmtpClient("smtp-mail.outlook.com", 587)
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential("", ""),
                    EnableSsl = true
                };
                return client.SendMailAsync(mailMessage);
            }
        }
}